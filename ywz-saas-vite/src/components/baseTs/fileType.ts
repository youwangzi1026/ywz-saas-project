export const FileTypeList = [
  { label: 'txt文档', value: 'text/plain' },
  { label: 'gif图片', value: 'image/gif' },
  { label: 'jpg图片', value: 'image/jpeg' },
  { label: 'png图片', value: 'image/png' },
  { label: 'pdf文件', value: 'application/pdf' },
  { label: 'word文档', value: 'application/msword' },
  { label: 'excel文档', value: 'application/vnd.ms-excel' },
  { label: 'XML文件', value: 'application/xml' }
]

