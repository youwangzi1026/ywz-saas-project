// 标签类型对应列表
export const labelTypeList = [
  { label: '地块组团', value: 'plot_group' },
  { label: '用地类型', value: 'plot_type' },
  { label: '服务器列表', value: 'servers' },
]
