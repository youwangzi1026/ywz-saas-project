import { JSEncrypt } from 'jsencrypt'
// npm install jsencrypt
const LOGIN_PUBLIC_KEY = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCGi0KZv3mdkNWQtf9Vz3d4HdoycOuBMyPB8F2CzqY/l+P7fig0o/iURkfL4fn75fUqqnuBRl5vqhUYI+4kXQq3W9oDfnTMado8tXiHcZdTqW4Xg9WF9Ft1SkrdXg7KY5GJtf/yGMPBmabAnRKg/Grh7JucX3cHhm6fBuj3Mbo6CQIDAQAB'

/**
 * 加密
 * @param text  需要加密的文本
 * @returns {string | false}
 */
export const encodeStr = (text: string) => {
  // RSA（非对称加密）
  const JSE = new JSEncrypt()
  // 设置公钥
  JSE.setPublicKey(LOGIN_PUBLIC_KEY)
  return JSE.encrypt(text)
}

