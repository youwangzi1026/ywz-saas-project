export const iconList: { value: string; label: string }[] = [
  {
    value: 'Setting',
    label: '设置',
  },
  {
    value: 'DataLine',
    label: '数据线',
  },
  {
    value: 'Notebook',
    label: '记事本',
  },
  {
    value: 'Message',
    label: '信息',
  },
  {
    value: 'Delete',
    label: '回收站',
  },
  {
    value: 'House',
    label: '首页',
  },
  {
    value: 'ChatDotRound',
    label: '对话',
  },
  {
    value: 'User',
    label: '用户',
  },
  {
    value: 'Star',
    label: '星星',
  },
  {
    value: 'Monitor',
    label: '显示器',
  },
  {
    value: 'DataBoard',
    label: '数据板',
  },
  {
    value: 'House',
    label: '主页'
  }
];
