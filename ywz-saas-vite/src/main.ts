import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
// element-plus相关
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn' // 引入element-plus中文语言包
// cookies插件
import VueCookies from 'vue3-cookies'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(VueCookies)
app.use(ElementPlus, {
  locale: zhCn,
})

app.mount('#app')
