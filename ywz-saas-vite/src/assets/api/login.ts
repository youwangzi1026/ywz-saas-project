import api from '../axios'

// 登录接口
export function loginApi(form: object) {
    return api.post('/system/login', form);
}

// 获取验证码
export function getCaptchaApi() {
    return api.get("/system/captcha?" + new Date().getTime(), {
        responseType: "blob"
    });
}

// 退出登录
export function logoutApi() {
    return api.get('/system/logout');
}

// 修改密码
export function resetPasswordApi(form: object) {
    return api.post('/system/resetPassword', form);
}

