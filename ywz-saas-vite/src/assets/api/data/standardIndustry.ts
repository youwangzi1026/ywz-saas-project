import api from '../../axios'

// 获取行业树
export function getStandardIndustryTreeApi() {
  return api.get('/standardIndustry/getStandardIndustryTree');
}

// 获取行业树分页
export function getStandardIndustryTreePageApi(form: any) {
  return api.get('/standardIndustry/getStandardIndustryTreePage', {params: form});
}

// 编辑行业
export function editStandardIndustryApi(form: any) {
  return api.post('/standardIndustry/editStandardIndustry', form);
}

// 删除行业
export function deleteStandardIndustryApi(id: number) {
  return api.delete('/standardIndustry/deleteStandardIndustry', {params: {id: id}})
}
