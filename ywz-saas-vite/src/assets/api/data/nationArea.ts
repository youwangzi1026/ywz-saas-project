import api from '../../axios'

// 根据id获取地区树
export function getNationAreaTreeListByIdApi(id: number, name: string) {
  return api.get('/nationArea/getNationAreaTreeListById', {
    params: {
      id: id,
      name: name
    }
  })
}

// 获取地区树分页
export function getNationAreaTreeListPageApi(form: any) {
  return api.get('/nationArea/getNationAreaTreeListPage', {
    params: form
  })
}

// 编辑地区
export function editNationAreaApi(form: any) {
  return api.post('/nationArea/editNationArea', form)
}

// 删除地区
export function deleteNationAreaApi(id: number) {
  return api.delete('/nationArea/deleteNationArea', {params: {id: id}})
}
