import api from '../../axios'

// 根据租户标识下载文件（不需要token）
export const downloadFileUrl = '/api/file/downloadFileByTenementKey?name='

// 查询文件分页
export function getFilePageApi(form: object) {
  return api.get('/file/getFilePage', { params: form })
}

// 上传文件
export function uploadFileApi(file: File, delFileName: string) {
  const formData = new FormData()
  formData.append('file', file)
  formData.append('delFileName', delFileName)
  return api.post('/file/uploadFile', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// 保存文件信息
export function editFileApi(form: any) {
  return api.post('/file/editFile', form)
}

// 下载文件
export function downloadFileApi(name: string, contentType: string) {
  return api.get('/file/downloadFile', { params: { name: name, contentType: contentType }, responseType: 'blob' })
}

// 删除服务器文件
export function deleteFileApi(name: string) {
  return api.delete('/file/deleteFile', { params: { name: name } })
}

// 删除文件信息
export function removeFileApi(ids: string) {
  return api.delete('/file/removeFile', { params: { ids: ids } })
}
