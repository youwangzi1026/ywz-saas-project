import api from '../../axios'

// 根据类型获取标签列表
export function getLabelListByTypeApi(type: string) {
    return api.get('/system/label/getLabelListByType', {params: {type: type}});
}

// 标签分页
export function getLabelPageApi(form: object) {
    return api.get('/system/label/getLabelPage', {params: form});
}

// 新增编辑标签
export function editLabelApi(form: object) {
    return api.post('/system/label/editLabel', form);
}

// 新增标签
export function deleteLabelApi(ids: string) {
    return api.delete('/system/label/removeLabel', {params: {ids: ids}});
}