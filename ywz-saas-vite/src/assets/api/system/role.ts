import api from "../../axios";

// 获取角色分页
export function getRolePageApi(form: object) {
    return api.get('/system/role/getRolePage', {params: form});
}

// 获取角色列表
export function getRoleListApi() {
    return api.get('/system/role/getRoleList');
}

// 新增编辑角色
export function editRoleApi(form: object) {
    return api.post('/system/role/editRole', form);
}

// 删除角色
export function deleteRoleApi(ids: string) {
    return api.delete('/system/role/removeRole', {params: {ids: ids}});
}