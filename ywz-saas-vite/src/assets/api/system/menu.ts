import api from '../../axios'

// 获取菜单列表
export function getMenuListApi() {
    return api.get("/system/menu/getMenuTree");
}

// 获取当前用户菜单列表
export function getCurrentMenuListApi(){
    return api.get("/system/menu/getCurrentMenuTree");
}

// 获取租户菜单列表
export function getTenementMenuListApi(key: string) {
    return api.get("/system/menu/getTenementMenuTree", {params: {key: key}});
}

// 保存菜单
export function saveMenuApi(data: string) {
    return api.post("/system/menu/saveMenu", {json: data});
}