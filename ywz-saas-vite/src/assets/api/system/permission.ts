import api from '../../axios'

// 获取权限列表
export function getPermissionPageApi(form: object) {
    return api.get('/system/permission/getPermissionPage', {params: form})
}

// 获取权限列表
export function getPermissionListApi() {
    return api.get('/system/permission/getPermissionList')
}

// 新增编辑权限
export function editPermissionApi(from: object){
    return api.post('/system/permission/editPermission', from)
}

// 删除权限
export function deletePermissionApi(ids: string) {
    return api.delete('/system/permission/removePermission', {params: {ids: ids}})
}
