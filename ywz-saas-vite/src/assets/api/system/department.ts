import api from '../../axios'

// 查询部门分页
export function getDepartmentPageApi(form: object) {
    return api.get('/department/getDepartmentPage', {params: form});
}

// 获取所有部门
export function getDepartmentListApi() {
    return api.get('/department/getDepartmentList');
}

// 添加编辑部门
export function editDepartmentApi(form: object) {
    return api.post('/department/editDepartment', form);
}

// 删除部门
export function deleteDepartmentApi(ids: string) {
    return api.delete('/department/removeDepartment', {params: {ids}});
}