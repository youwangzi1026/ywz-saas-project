import api from '../../axios'

// 查询用户登录日志分页
export function getLoginLogPageApi(form: object) {
    return api.get('/system/sysLoginLog/getLoginLogPage', {params: form})
}

// 删除用户登录日志
export function deleteLoginLogApi(ids: string) {
    return api.delete('/system/sysLoginLog/removeLoginLog', {params: {ids: ids}})
}

// 查询操作日志分页
export function getOperLogPageApi(form: object) {
    return api.get('/system/operLog/getOperLogPage', {params: form})
}

export function deleteOperLogApi(ids: string) {
    return api.delete('/system/operLog/removeOperLog', {params: {ids: ids}})
}