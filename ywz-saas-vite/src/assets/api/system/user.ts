import api from '../../axios'

// 获取用户分页
export function getUserPageApi(form: object) {
    return api.get(`/system/user/getUserPage`, {params: form});
}

// 添加编辑用户
export function editUserApi(form: object) {
    return api.post(`/system/user/editUser`, form);
}

// 删除用户
export function deleteUserApi(ids: string) {
    return api.delete(`/system/user/removeUser`, {params: {ids: ids}});
}