import api from '../../axios'

// 获取租户分页列表
export function getTenementPageApi(form: object) {
    return api.get('/tenement/getTenementPage', {params: form})
}

// 新增、编辑租户
export function editTenementApi(form: object) {
    return api.post('/tenement/editTenement', form)
}

// 删除租户
export function deleteTenementApi(ids: string) {
    return api.delete('/tenement/removeTenement', {params: {ids: ids}})
}

export function getTenementListApi() {
    return api.get('/tenement/getTenementList')
}
