import api from '../../axios'

// 根据菜单id获取菜单子集列表
export function getMenuSubListByRemarkApi(remark: string) {
  return api.get("/menuSub/getMenuSubListByRemark", {
    params: {
      remark: remark
    }
  })
}

// 保存菜单子表
export function saveMenuSubApi(data: string) {
  return api.post("/menuSub/saveMenuSub", {json: data});
}
