import axios from 'axios'
import {useCookies} from "vue3-cookies";
const {cookies} = useCookies();

//创建一个axios对象
const api = axios.create({
  baseURL: '/api',//会在发送请求时候拼接在url参数前面
  timeout: 5000//请求5s超时
})

api.defaults.withCredentials = true; // 全局允许带cookie
api.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'; // 全局设置请求头

// 添加请求拦截器
api.interceptors.request.use(function (config) {
  // 如果存在token则在请求头中添加
  let token = cookies.get("userToken");
  if (token != null)
    config.headers['token'] = token;
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
api.interceptors.response.use(function (response) {
  // 2xx响应码对响应数据做点什么
  return response;
}, function (error) {
  if (error.status === 401) {
    cookies.remove("token");
    cookies.remove("name");
    //跳转到登录页面
    window.location.href = '/login'
  }
  return Promise.reject(error);
});

export default api;
