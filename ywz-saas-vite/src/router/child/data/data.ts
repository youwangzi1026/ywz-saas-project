import type {RouteRecordRaw} from "vue-router";
import Data from "@/views/data/Data.vue";
import Label from "@/views/data/label/Label.vue";
import File from "@/views/data/file/File.vue";
import Industry from "@/views/data/standardIndustry/StandardIndustry.vue";
import China from "@/views/data/nationArea/NationArea.vue";

// 路由页面地址数组
const system = [
  {path: "/data", redirect: "/data/label"}, // 默认
  {
    path: "/data", component: Data, children: <RouteRecordRaw[]>[
      {path: "/data/label", component: Label}, // 标签管理
      {path: '/data/file', component: File}, // 文件管理
      {path: "/data/industry", component: Industry}, // 国民行业
      {path: '/data/china', component: China}, // 中国地区
    ]
  },
]

export default system;
