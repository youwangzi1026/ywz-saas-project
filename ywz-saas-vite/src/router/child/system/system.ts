import System from '@/views/system/System.vue';
import type {RouteRecordRaw} from "vue-router";
import User from "@/views/system/user/User.vue";
import Permission from "@/views/system/permission/Permission.vue";
import MenuTabs from "@/views/system/menu/MenuTabs.vue";
import Department from "@/views/system/department/Department.vue";
import Role from "@/views/system/role/Role.vue";
import Log from "@/views/system/log/Log.vue";
import Tenement from "@/views/system/tenement/Tenement.vue";

// 路由页面地址数组
const system = [
  {path: "/system", redirect: "/system/user"}, // 默认
  {
    path: "/system", component: System, children: <RouteRecordRaw[]>[
      {path: "/system/tenement", component: Tenement}, // 租户管理
      {path: "/system/user", component: User}, // 用户管理
      {path: "/system/permission", component: Permission}, // 权限管理
      {path: "/system/menuTabs", component: MenuTabs}, // 菜单管理
      {path: "/system/department", component: Department}, // 部门管理
      {path: "/system/role", component: Role}, // 角色管理
      {path: "/system/log", component: Log}, // 角色管理
    ]
  },
]

export default system;
