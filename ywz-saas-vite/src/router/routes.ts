import Login from '@/views/Login.vue'
import Index from '@/views/Index.vue'
import type {RouteRecordRaw} from "vue-router";
import system from "./child/system/system"
import data from "./child/data/data";
// 路由页面地址数组
const routes: Array<RouteRecordRaw> = [
  {path: '/', redirect: '/login'}, // 默认
  {path: '/login', component: Login},
  {
    path: '/index', component: Index, children: <RouteRecordRaw[]>[
      ...system, // 系统管理
      ...data // 数据管理
    ]
  }
]

export default routes;
