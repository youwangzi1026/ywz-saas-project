import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import vueDevTools from 'vite-plugin-vue-devtools'

// https://vite.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    vueDevTools(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    },
  },
  css: { // 解决element plus警告
    preprocessorOptions: {
      scss: {api: 'modern-compiler'},
    }
  },
  server: {
    proxy: { //用代理的方式实现跨域访问
      '/api': {
        target: 'http://localhost:8087',
        changeOrigin: true, //是否改变域名
        secure: false,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  }
})
