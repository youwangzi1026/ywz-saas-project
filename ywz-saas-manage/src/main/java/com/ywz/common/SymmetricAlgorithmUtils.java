package com.ywz.common;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

/**
 * 类描述 -> 对称加密工具类
 *
 * @Author: ywz
 * @Date: 2025/01/02
 */
public class SymmetricAlgorithmUtils {
    // 加密算法
    private static final SymmetricAlgorithm ALGORITHM = SymmetricAlgorithm.AES;
    private static final String KEY = "YouWangZi1234567";
    private static final SymmetricCrypto aes = new SymmetricCrypto(ALGORITHM, KEY.getBytes(CharsetUtil.CHARSET_UTF_8));


    /**
     * 方法描述 -> 加密
     *
     * @param content 待加密内容
     * @Author: ywz
     * @Date: 2025/01/02
     */
    public static String encrypt(String content) {
        return aes.encryptHex(content, CharsetUtil.CHARSET_UTF_8);
    }

    /**
     * 方法描述 -> 解密
     *
     * @param content 待解密内容
     * @Author: ywz
     * @Date: 2025/01/02
     */
    public static String decrypt(String content) {
        return aes.decryptStr(content, CharsetUtil.CHARSET_UTF_8);
    }
}
