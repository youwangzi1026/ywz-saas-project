package com.ywz.common;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 类描述 -> 响应结果工具类
 *
 * @Author: ywz
 * @Date: 2024/09/17
 */
@Data
@Schema(description = "响应结果工具类")
@AllArgsConstructor
public class ResultResp {

    @Schema(description = "页码")
    private Integer code;

    @Schema(description = "响应数据")
    private Object data;

    public static final int SUCCESS_CODE = 200; // 200表示成功
    public static final int ERROR_CODE = 500; // 500表示失败
    public static final int UNAUTHORIZED_CODE = 401; // 401表示未授权

    public static ResultResp success() {
        return new ResultResp(SUCCESS_CODE, "成功");
    }

    public static ResultResp success(Object data) {
        return new ResultResp(SUCCESS_CODE, data);
    }

    public static ResultResp error() {
        return new ResultResp(ERROR_CODE, "失败");
    }

    public static ResultResp error(Object data) {
        return new ResultResp(ERROR_CODE, data);
    }
}
