package com.ywz.common;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

/**
 * 类描述 -> 非对称加密算法工具类 RSA
 *
 * @Author: ywz
 * @Date: 2025/01/22
 */
public class AsymmetricCryptoUtils {
    private static final String PRIVATE_KEY = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAIaLQpm/eZ2Q1ZC1/1XPd3gd2jJw64EzI8HwXYLOpj+X4/t+KDSj+JRGR8vh+fvl9Sqqe4FGXm+qFRgj7iRdCrdb2gN+dMxp2jy1eIdxl1OpbheD1YX0W3VKSt1eDspjkYm1//IYw8GZpsCdEqD8auHsm5xfdweGbp8G6PcxujoJAgMBAAECgYAg/i9j0n95MxYb+XVWzpxmWTOzoNsvgKHpIg9cT16XQ6EFgAdN+VOw0Jc0mQKbUeXQA6RrrBzmVxf7Jnx0cFh7CJ5tcnHTncnYyX5uRJPppea/o/e63Jtk3XcxmKXvIBfma4gNdttdh2hDUJ2vfYsJ9rKzPgk8erpVr2+4+G733QJBANFEIcBtUFCmNCzvCOe++lQ6YKjrz7TSfCucvtd5jUmNJsAjCgNwX5njUiK4kogYPSdbCXZXQIdQdgzMCrp63lsCQQCklzgEevCABQRKbNtwJfP2LWXLnWnIOG1PfJP9saTtr3mjCfRp/iOPTkaf7dS9JKnfQRWlj28A67ilqko1E/5rAkALvbPbFQcFj12sC7lHL1riMOhNcfWUYp7e34uBkWtQ7h93dj/Qk5R5kNyB4DMweZOsypnMTvbq4KPO8f/EGLKzAkA5drZPEiEfgSDjXygp+oY3f9gXg8jn55N6efMtUrVTar1cB+C2lM0Tfm+37JAmzUMluBJ3sCFAQAxCgrbP20ArAkAHRJLwFXjIzNDmMkllGo5H8iQm8ocwk4O1Q9h7zorJx9QDpjN5M6X9X0ttA60aeBvijnYjjg965M5T7yKluFuI";
    private static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCGi0KZv3mdkNWQtf9Vz3d4HdoycOuBMyPB8F2CzqY/l+P7fig0o/iURkfL4fn75fUqqnuBRl5vqhUYI+4kXQq3W9oDfnTMado8tXiHcZdTqW4Xg9WF9Ft1SkrdXg7KY5GJtf/yGMPBmabAnRKg/Grh7JucX3cHhm6fBuj3Mbo6CQIDAQAB";
    private static final RSA rsa = new RSA(PRIVATE_KEY, PUBLIC_KEY);

    /**
     * 方法描述 ->  加密
     *
     * @param content 待加密内容
     * @param key 秘钥类型
     * @Author: ywz
     * @Date: 2025/01/22
     */
    public static String encrypt(String content, KeyType key) {
        byte[] encrypt = rsa.encrypt(StrUtil.bytes(content, CharsetUtil.CHARSET_UTF_8), key);
        return Base64.encode(encrypt);
    }

    /**
     * 方法描述 ->  解密
     *
     * @param content 待解密内容
     * @param key 秘钥类型
     * @Author: ywz
     * @Date: 2025/01/22
     */
    public static String decrypt(String content, KeyType key) {
        System.out.println(content);
        byte[] decrypt = rsa.decrypt(content, key);
        return new String(decrypt, CharsetUtil.CHARSET_UTF_8);
    }
}
