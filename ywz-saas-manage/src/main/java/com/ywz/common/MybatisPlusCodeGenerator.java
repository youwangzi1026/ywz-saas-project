package com.ywz.common;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

/**
 * 类描述 -> mybatis-plus代码生成器 （新版本mybatis-plus版本3.5.1以上）
 *
 * @Author: ywz
 * @Date: 2025/02/07
 */
public class MybatisPlusCodeGenerator {
    public static final String URL = "jdbc:mysql://localhost:3306/saas-crm-master?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "root";
    // 生成包路径
    private static final String PACKAGE_NAME = "com.ywz.project.base.data";
    // 生成表数组
    private static final String[] TABLE_NAMES = {"t_data_nation_area"
};
    // 是否启动
    private static final boolean START = true;

    public static void main(String[] args) {
        if (!START)
            return;

        FastAutoGenerator.create(URL, USERNAME, PASSWORD)
                .globalConfig(builder -> {
                    builder.author("游王子og") // 设置作者
                            .outputDir("./code"); // 输出目录
                })
                .packageConfig(builder -> {
                    builder.parent(PACKAGE_NAME) // 设置父包名
                            .entity("entity") // 设置实体类包名
                            .mapper("mapper") // 设置 Mapper 接口包名
                            .service("service") // 设置 Service 接口包名
                            .serviceImpl("service.impl") // 设置 Service 实现类包名
                            .xml("mappers"); // 设置 Mapper XML 文件包名
                })
                .strategyConfig(builder -> {
                    builder.addInclude(TABLE_NAMES) // 设置需要生成的表名
                            .entityBuilder()
                            .javaTemplate("/templates/entity.java")
                            .enableLombok() // 启用 Lombok
                            .enableTableFieldAnnotation() // 启用字段注解
                            .controllerBuilder()
                            .enableRestStyle(); // 启用 REST 风格
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用 Freemarker 模板引擎
                .execute(); // 执行生成
    }
}
