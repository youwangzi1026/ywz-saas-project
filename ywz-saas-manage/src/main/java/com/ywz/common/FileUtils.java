package com.ywz.common;

import cn.hutool.jwt.JWT;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

/**
 * 类描述 -> 文件工具类
 *  isImage(MultipartFile file) 检查文件是否是图片类型
 *  isExcel(MultipartFile file) 检查文件是否是excel类型
 *  checkFileType(MultipartFile file, String... type) 检查文件是否是指定的类型
 *  uploadFile(MultipartFile file) 上传文件
 *  downloadFile(HttpServletResponse response, String fileName) 下载文件
 *  deleteFile(String fileName) 删除文件
 * @Author: ywz
 * @Date: 2024/11/08
 */
@Slf4j
public class FileUtils {

    private static final String FILE_PATH = "D:\\file\\";
    public static final String[] IMAGES = {".png", ".jpg", ".jpeg", ".gif", ".bmp", ".webp", ".svg", ".ico"};

    /**
     * 方法描述 -> 检查文件是否是图片类型
     *
     * @param file 文件
     * @Author: ywz
     * @Date: 2024/11/08
     */
    public static boolean isImage(MultipartFile file) {
        return checkFileType(file, IMAGES);
    }

    /**
     * 方法描述 -> 检查文件是否是excel类型
     *
     * @param file 文件
     * @Author: ywz
     * @Date: 2024/11/08
     */
    public static boolean isExcel(MultipartFile file) {
        return checkFileType(file, ".xlsx", ".xls");
    }

    /**
     * 方法描述 ->  检查文件是否是指定的类型
     *
     * @param file 文件
     * @param type 文件类型
     * @Author: ywz
     * @Date: 2024/10/23
     */
    public static boolean checkFileType(MultipartFile file, String... type) {
        if (file == null) return false;
        String originalFilename = file.getOriginalFilename();
        if (StringUtils.isEmpty(originalFilename)) return false;
        for (String s : type) {
            if (originalFilename.endsWith(s))
                return true;
        }
        return false;
    }

    /**
     * 方法描述 -> 上传文件
     *
     * @param file 文件
     * @Author: ywz
     * @Date: 2024/11/08
     */
    public static ResultResp uploadFile(MultipartFile file) {
        if (file == null)
            return ResultResp.error("文件为空");
        String originalFilename = file.getOriginalFilename();
        if (StringUtils.isEmpty(originalFilename))
            return ResultResp.error("文件名不能为空");
        //获取原文件的类型
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        //使用UUID重新生成文件名，防止文件名称重复造成文件覆盖
        String fileName = UUID.randomUUID() + suffix;
        // 获取当前租户标识
        String tenementKey = getTenementKey();
        if (StringUtils.isEmpty(tenementKey))
            return ResultResp.error("租户标识为空");
        String filePath = FILE_PATH + tenementKey + "/";
        //创建一个目录对象，用于存储上传文件
        File dir = new File(filePath);
        //判断当前目录是否存在：目录不存在，需要创建
        boolean mkdirs = dir.mkdirs();
        log.info("创建目录：{}", mkdirs);
        try {
            //保存文件到指定目录
            file.transferTo(new File(filePath + fileName));
            return ResultResp.success(fileName);
        } catch (IOException e) {
            log.error("文件上传失败", e);
            return ResultResp.error("文件上传失败");
        }
    }

    /**
     * 方法描述 -> 下载文件
     *
     * @param fileName 文件名
     * @param resp 响应
     * @param contentType 文件类型，比如：image/png（png图片）
     * @Author: ywz
     * @Date: 2024/11/08
     */
    public static ResultResp downloadFile(String fileName, String contentType, HttpServletResponse resp) {
        return downloadFile(fileName, contentType, getTenementKey(), resp);
    }

    /**
     * 方法描述 -> 下载文件
     *
     * @param fileName 文件名
     * @param resp 响应
     * @param contentType 文件类型，比如：image/png（png图片）
     * @Author: ywz
     * @Date: 2024/11/08
     */
    public static ResultResp downloadFile(String fileName, String contentType, String key, HttpServletResponse resp) {
        if (StringUtils.isEmpties(fileName, key, contentType))
            return ResultResp.error("文件名类型租户标识不能为空");
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            File file = new File(FILE_PATH + key + "/" + fileName);
            if (!file.exists())
                return ResultResp.error("文件不存在");
            if (resp == null)
                return ResultResp.error("响应对象为空");
            //输入流，通过输入流读取文件内容
            bis = new BufferedInputStream(Files.newInputStream(file.toPath()));
            //输出流，通过输出流将文件写回浏览器
            bos = new BufferedOutputStream(resp.getOutputStream());
            // 设置相应类型
            resp.setContentType(contentType);
            int i = 0;
            while ((i = bis.read()) != -1) {
                bos.write(i);
            }
            bos.flush();
        } catch (Exception e) {
            log.error("文件下载失败", e);
        } finally {
            try {
                if (bos != null) bos.close();
                if (bis != null) bis.close();
            } catch (IOException e) {
                log.error("文件流关闭失败", e);
            }
        }
        return null;
    }

    /**
     * 方法描述 -> 删除文件
     *
     * @param fileName 文件名
     * @Author: ywz
     * @Date: 2024/11/08
     */
    public static ResultResp deleteFile(String fileName) {
        if (StringUtils.isEmpty(fileName))
            return ResultResp.error("文件名为空");
        String tenementKey = getTenementKey();
        if (StringUtils.isEmpty(tenementKey))
            return ResultResp.error("租户标识为空");
        File file = new File(FILE_PATH + tenementKey + "/" + fileName);
        if (!file.exists())
            return ResultResp.error("文件不存在");
        if (!file.delete())
            return ResultResp.error("文件删除失败");
        return ResultResp.success("文件删除成功");
    }

    /**
     * 方法描述 -> 获取当前租户的文件路径
     *
     * @Author: ywz
     * @Date: 2025/01/07
     */
    private static String getTenementKey() {
        JWT currentTokenInfo = JWTUtils.getCurrentTokenInfo();
        if (currentTokenInfo == null)
            return null;
        Object username = currentTokenInfo.getPayload("username");
        if (username == null)
            return null;
        return username.toString().split("_")[0];
    }
}
