package com.ywz.common;

import java.time.LocalDate;

/**
 * 类描述 -> ID生成器
 *
 * @Author: ywz
 * @Date: 2025/02/20
 */
public class IDGenerator {
    /**
     * 方法描述 -> 生成ID
     *
     * @param prefix 前缀
     * @Author: ywz
     * @Date: 2025/02/20
     */
    public static String generateId(String prefix) {
        String year = String.valueOf(LocalDate.now().getYear());
        String month = String.valueOf(LocalDate.now().getMonthValue());
        String day = String.valueOf(LocalDate.now().getDayOfMonth());
        if (month.length() == 1)
            month = "0" + month;
        if (day.length() == 1)
            day = "0" + day;
        if (StringUtils.isEmpty(prefix))
            return year + month + day + "_" + System.nanoTime();
        return prefix + year + month + day + System.nanoTime();
    }
}
