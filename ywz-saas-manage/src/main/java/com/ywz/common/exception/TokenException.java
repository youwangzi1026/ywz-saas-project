package com.ywz.common.exception;

/**
 * 类描述 -> token异常
 *
 * @Author: ywz
 * @Date: 2025/02/21
 */
public class TokenException extends RuntimeException{

    public TokenException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
