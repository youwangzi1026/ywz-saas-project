package com.ywz.common;

import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

/**
 * 类描述 -> 自定义字符串工具类
 * 此工具类暂时包含如下功能：
 * isEmpty()判断字符串是否为空
 * isEmpties()判断多个字符串是否为空
 * isNull()判断对象是否为null，非String方法，参数为Object类型
 * subSpecifiedString()判断字符串是否超出指定长度，超出则截取到指定长度
 * yearMonthToDate()将年月的字符串转成年月日格式
 * yearMonthToDateTime()将年月的字符串转成年月日时分秒格式
 * checkFileType()检查文件是否是以指定的类型结尾
 * isNumeric() 判断字符串是否为数值
 * isPhone() 判断字符串是否为手机号
 * isEmail() 判断字符串是否为邮箱
 * isDate() 判断字符串是否为日期，格式例如：2024-10-28
 * isDateTime() 判断字符串格式是否为日期时间，格式例如：2024-10-28 09:45:00
 * isOverLength() 判断字符串是否超出指定长度
 * @Author: ywz
 * @Date: 2024/09/16
 */
@Slf4j
public class StringUtils {
    // 字符串分隔符
    public static final String SEPARATOR = ",";

    /**
     * 方法描述 -> 判断字符串是否为空
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/09/16
     */
    public static boolean isEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * 方法描述 -> 判断多个字符串是否为空
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/10/27
     */
    public static boolean isEmpties(String... str) {
        if (str == null || str.length == 0)
            return true;
        for (String s : str) {
            if (isEmpty(s))
                return true;
        }
        return false;
    }

    /**
     * 方法描述 -> 判断对象是否为空
     *
     * @param objects 对象
     * @Author: ywz
     * @Date: 2024/10/28
     */
    public static boolean isNull(Object... objects) {
        for (Object object : objects) {
            if (object == null)
                return true;
        }
        return false;
    }

    /**
     * 方法描述 -> 判断字符串是否超出指定长度，超出则截取到指定长度
     *
     * @param str    字符串
     * @param length 指定长度
     * @Author: ywz
     * @Date: 2024/09/16
     */
    public static String subSpecifiedString(String str, int length) {
        if (isEmpty(str)) {
            log.error("字符串不能为空");
            return null;
        }
        return str.length() > length ? str.substring(0, length) : str;
    }

    /**
     * 方法描述 -> 将年月的字符串转成年月日格式
     *
     * @param yearMonth 年月
     * @Author: ywz
     * @Date: 2024/09/16
     */
    public static String yearMonthToDate(String yearMonth) {
        if (isEmpty(yearMonth)) {
            log.error("日期不能为空");
            return null;
        }
        Pattern pattern = Pattern.compile("^[0-9]{4}-(0[1-9]|1[0-2])$");
        if (!pattern.matcher(yearMonth).matches()) {
            log.error("日期格式错误");
            return null;
        }
        return yearMonth + "-01";
    }

    /**
     * 方法描述 -> 将年月的字符串转成年月日时分秒格式
     *
     * @param yearMonth 年月
     * @Author: ywz
     * @Date: 2024/09/16
     */
    public static String yearMonthToDateTime(String yearMonth) {
        if (isEmpty(yearMonth)) {
            log.error("日期不能为空");
            return null;
        }
        Pattern pattern = Pattern.compile("^[0-9]{4}-(0[1-9]|1[0-2])$");
        if (!pattern.matcher(yearMonth).matches()) {
            log.error("日期格式错误");
            return null;
        }
        return yearMonth + "-01 00:00:00";
    }


    /**
     * 方法描述 -> 判断字符串是否为数值类型
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/10/28
     */
    public static boolean isNumeric(String str) {
        if (isEmpty(str)) return false;
        return str.matches("-?[0-9]+.?[0-9]*");
    }

    /**
     * 方法描述 -> 判断字符串是否为数值类型和分隔符拼接
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/11/08
     */
    public static boolean isNumberAndSeparator(String str) {
        if (isEmpty(str)) return false;
        return str.matches("-?[0-9]+(" + StringUtils.SEPARATOR + "-?[0-9]+)*" + StringUtils.SEPARATOR + "?");
    }

    /**
     * 方法描述 -> 判断字符串是否为手机号
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/10/28
     */
    public static boolean isPhone(String str) {
        if (isEmpty(str)) return false;
        return str.matches("^[1][3456789][0-9]{9}$");
    }

    /**
     * 方法描述 -> 判断字符串是否为邮箱
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/10/28
     */
    public static boolean isEmail(String str) {
        if (isEmpty(str)) return false;
        return str.matches("^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$");
    }

    /**
     * 方法描述 -> 判断字符串是否为日期
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/10/28
     */
    public static boolean isDate(String str) {
        if (isEmpty(str)) return false;
        return str.matches("^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$");
    }

    /**
     * 方法描述 -> 判断字符串是否为日期时间
     *
     * @param str 字符串
     * @Author: ywz
     * @Date: 2024/10/28
     */
    public static boolean isDateTime(String str) {
        if (isEmpty(str)) return false;
        return str.matches("^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])T(0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9]):(0[0-9]|[1-5][0-9])$");
    }


    /**
     * 方法描述 -> 判断字符串是否超出指定长度
     * 如果字符串为null则返回false
     * 如果字符串长度大于指定长度，则返回true
     *
     * @param length 长度
     * @param str 字符串列表
     * @Author: ywz
     * @Date: 2024/12/02
     */
    public static boolean isOverLength(int length, String... str) {
        if (str == null) return false;
        for (String s : str)
            if (!isEmpty(s) && s.length() > length)
                return true;
        return false;
    }
}