package com.ywz.framework.security.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ywz.common.ResultResp;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * 类描述 -> 认证失败处理类
 *
 * @Author: ywz
 * @Date: 2024/07/28
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {
    /**
     * 方法描述 -> 认证失败处理
     *
     * @Author: ywz
     * @Date: 2024/07/28
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        response.setStatus(200);
        response.setContentType("application/json;charset=UTF-8");
        Map<String, Object> result = new HashMap<>();
        result.put("data", authException.getMessage());
        result.put("code", ResultResp.UNAUTHORIZED_CODE);
        String s = new ObjectMapper().writeValueAsString(result);
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(s.getBytes(StandardCharsets.UTF_8));
        outputStream.close();
    }
}