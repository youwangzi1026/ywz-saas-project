package com.ywz.framework.security;

import com.ywz.project.base.system.entity.TSysUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * 类描述 -> Spring Security 自定义用户实体类
 *
 * @Author: ywz
 * @Date: 2024/09/27
 */
@Setter
@Getter
public class CustomUser extends User {
    private TSysUser sysUser;

    public CustomUser(TSysUser sysUser, Collection<? extends GrantedAuthority> authorities) {
        super(sysUser.getUsername(), sysUser.getPassword(), authorities);
        this.sysUser = sysUser;
    }
}
