package com.ywz.framework.mybatisplus.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * 类描述 -> 初始化库Mapper
 *
 * @Author: ywz
 * @Date: 2024/11/19
 */
@Mapper
public interface DatabaseMapper {
    /**
     * 方法描述 -> 创建数据库
     *
     * @param databaseName 数据库名称
     * @Author: ywz
     * @Date: 2024/11/19
     */
    @Select("CREATE DATABASE IF NOT EXISTS ${databaseName}")
    void createDatabase(String databaseName);

    /**
     * 方法描述 -> 选择数据库
     *
     * @param databaseName 数据库名称
     * @Author: ywz
     * @Date: 2024/11/19
     */
    @Select("USE ${databaseName}")
    void useDatabase(String databaseName);

    /**
     * 方法描述 -> 创建管理员账号
     *
     * @param adminAccount 管理员账号
     * @param password 密码
     * @Author: ywz
     * @Date: 2024/11/19
     */
    @Insert("INSERT INTO t_sys_user (id,name,username,password,status,is_deleted) VALUES " +
            "(-1,'超级管理员',#{adminAccount},#{password},1,0)")
    void saveAdminAccount(String adminAccount, String password);

    /**
     * 方法描述 -> 添加租户标识
     *
     * @param key 租户标识
     * @Author: ywz
     * @Date: 2024/11/19
     */
    @Insert("insert into t_data_label(name,tag,type,is_deleted) values (#{tenementSimpleName},#{key},'tenement_key',0)")
    void saveTenementLabel(String key, String tenementSimpleName);

    /**
     * 方法描述 -> 绑定角色
     *
     * @Author: ywz
     * @Date: 2024/11/19
     */
    @Insert("insert into t_sys_user_role(user_id,role_id,is_deleted) values (-1,-1,0)")
    void saveAdminUserRole();

    /**
     * 方法描述 -> 添加管理员角色
     *
     * @Author: ywz
     * @Date: 2024/11/19
     */
    @Insert("insert into t_sys_role(id,role_name,role_type,status,is_deleted) values (-1,'超级管理员','ROLE_ADMIN',1,0)")
    void saveAdminRole();
}
