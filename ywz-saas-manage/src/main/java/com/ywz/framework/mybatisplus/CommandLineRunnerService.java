package com.ywz.framework.mybatisplus;

import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.creator.DataSourceProperty;
import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ywz.common.SymmetricAlgorithmUtils;
import com.ywz.project.base.system.entity.TSysTenement;
import com.ywz.project.base.system.service.TSysTenementService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

/**
 * 类描述 -> 启动系统时初始化数据库配置
 *
 * @Author: ywz
 * @Date: 2024/11/17
 */
@Component
public class CommandLineRunnerService implements CommandLineRunner {
    @Resource
    private TSysTenementService tenementService;
    @Resource
    private DataSource dataSource;
    @Resource
    private DefaultDataSourceCreator dataSourceCreator;
    @Value("${tenement.datasource.driver-class-name}")
    private String driverClassName;
    @Value("${tenement.datasource.database-prefix}")
    private String databasePrefix;
    @Value("${tenement.datasource.url-suffix}")
    private String urlSuffix;

    @Override
    public void run(String... args) {
        System.out.println("==================初始化租户数据库配置==================");
        LambdaQueryWrapper<TSysTenement> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TSysTenement::getIsDeleted, 0);
        List<TSysTenement> list = tenementService.list(wrapper);
        DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
        for (TSysTenement TSysTenement : list) {
            // 配置数据源
            DataSourceProperty dataSourceProperty = new DataSourceProperty();
            dataSourceProperty.setLazy(true);
            dataSourceProperty.setUsername(SymmetricAlgorithmUtils.decrypt(TSysTenement.getDbUsername()));
            dataSourceProperty.setPassword(SymmetricAlgorithmUtils.decrypt(TSysTenement.getDbPassword()));
            String url = TSysTenement.getDbUrl();
            if (!url.contains(":"))
                url = url + ":3306"; // 使用默认端口号
            dataSourceProperty.setUrl("jdbc:mysql://" + url + "/" + databasePrefix + TSysTenement.getTenantKey() + urlSuffix);
            dataSourceProperty.setDriverClassName(driverClassName);
            ds.addDataSource(TSysTenement.getTenantKey(), dataSourceCreator.createDataSource(dataSourceProperty));
        }
    }
}
