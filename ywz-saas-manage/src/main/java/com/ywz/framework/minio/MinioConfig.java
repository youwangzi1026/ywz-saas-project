package com.ywz.framework.minio;

import com.ywz.common.SymmetricAlgorithmUtils;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 类描述 -> minio配置类
 *
 * @Author: ywz
 * @Date: 2025/02/11
 */
@Configuration
public class MinioConfig {
    @Value("${minio.endpoint}")
    private String endpoint;
    @Value("${minio.accessKey}")
    private String accessKey;
    @Value("${minio.secretKey}")
    private String secretKey;

    /**
     * 方法描述 -> 获取minio客户端minio客户端
     *
     * @Author: ywz
     * @Date: 2025/02/11
     */
    @Bean
    public MinioClient getMinioClient() {
        return MinioClient.builder()
                .endpoint(SymmetricAlgorithmUtils.decrypt(endpoint))
                .credentials(SymmetricAlgorithmUtils.decrypt(accessKey)
                        , SymmetricAlgorithmUtils.decrypt(secretKey))
                .build();
    }
}
