package com.ywz.framework.sso;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * 类描述 -> 统一门户登录认证管理器
 *
 * @Author: ywz
 * @Date: 2024/11/05
 */
@Slf4j
public class SSOAuthenticationManager  implements AuthenticationManager, ApplicationContextAware {

    private RestTemplate restTemplate;

    /**
     * 方法描述 -> 设置程序上下文。
     *<p>
     *     此方法会在程序启动时执行一次，并且只会执行一次，因此可以做一些全局的初始化操作。
     *</p>
     * @param applicationContext 程序上下文
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    }

    /**
     * 方法描述 -> 认证
     *
     * @param authentication 认证信息
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.debug("开始统一门户登录认证");
        // 定义sso请求路径与参数
        String tokenUri = "/oauth/token?client_id={clientId}&client_secret={clientSecret}&grant_type={grantType}&scope=all";
        // 远程调用携带的参数集合
        Map<String, Object> userAccessTokenUriVariables = new HashMap<>();
        // 根据具体的用户实例拼接不同的参数信息
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            // 用户名密码认证
        }else{
            // 其他认证方式
        }
        // new UsernamePasswordAuthenticationToken(user, authentication.getCredentials(), restRoles);
        return null;
    }
}
