package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysOperLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysOperLogService extends IService<TSysOperLog> {

}
