package com.ywz.project.base.data.mapper;

import com.ywz.project.base.data.entity.TDataNationArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * <p>
 * 国家行政区划信息表 Mapper 接口
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-28
 */
public interface TDataNationAreaMapper extends BaseMapper<TDataNationArea> {

}

