package com.ywz.project.base.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ywz.project.base.system.entity.TSysTenement;

/**
 * <p>
 * 租户信息表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-11-15
 */
public interface TSysTenementService extends IService<TSysTenement> {

}
