package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-11-21
 */
public interface TSysDepartmentMapper extends BaseMapper<TSysDepartment> {

}
