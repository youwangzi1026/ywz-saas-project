package com.ywz.project.base.data.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ywz.project.base.data.entity.TDataFile;

/**
 * <p>
 * 系统文件 服务类
 * </p>
 *
 * @author ywz
 * @since 2025-01-06
 */
public interface TDataFileService extends IService<TDataFile> {

}
