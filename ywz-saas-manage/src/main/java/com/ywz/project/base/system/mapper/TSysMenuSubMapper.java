package com.ywz.project.base.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ywz.project.base.system.entity.TSysMenuSub;


/**
 * <p>
 * 菜单子表 Mapper 接口
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-18
 */
public interface TSysMenuSubMapper extends BaseMapper<TSysMenuSub> {

}

