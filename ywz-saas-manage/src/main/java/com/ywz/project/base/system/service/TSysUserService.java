package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-11-14
 */
public interface TSysUserService extends IService<TSysUser> {

}
