package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-11-14
 */
public interface TSysUserMapper extends BaseMapper<TSysUser> {

}
