package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysLoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登录日志表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysLoginLogMapper extends BaseMapper<TSysLoginLog> {

}
