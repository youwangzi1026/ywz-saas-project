package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 登录日志表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysLoginLogService extends IService<TSysLoginLog> {

}
