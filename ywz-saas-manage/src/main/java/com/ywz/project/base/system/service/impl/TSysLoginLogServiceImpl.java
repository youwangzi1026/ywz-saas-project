package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysLoginLog;
import com.ywz.project.base.system.mapper.TSysLoginLogMapper;
import com.ywz.project.base.system.service.TSysLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录日志表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
@Service
public class TSysLoginLogServiceImpl extends ServiceImpl<TSysLoginLogMapper, TSysLoginLog> implements TSysLoginLogService {

}
