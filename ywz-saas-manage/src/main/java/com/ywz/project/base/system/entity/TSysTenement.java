package com.ywz.project.base.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serial;
import io.swagger.v3.oas.annotations.media.Schema;

/**
* <p>
* 租户信息表
* </p>
*
* @author 游王子og
* @since 2025-02-21
*/
@Getter
@Setter
@ToString
@TableName("t_sys_tenement")
public class TSysTenement implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    @Schema(description = "主键")
    private Integer id;

    /**
     * 租户名
     */
    @TableField("NAME")
    @Schema(description = "租户名")
    private String name;

    /**
     * 租户简称
     */
    @TableField("SIMPLE_NAME")
    @Schema(description = "租户简称")
    private String simpleName;

    /**
     * 分库标识
     */
    @TableField("TENANT_KEY")
    @Schema(description = "分库标识")
    private String tenantKey;

    /**
     * 分库地址
     */
    @TableField("DB_URL")
    @Schema(description = "分库地址")
    private String dbUrl;

    /**
     * 分库帐号
     */
    @TableField("DB_USERNAME")
    @Schema(description = "分库帐号")
    private String dbUsername;

    /**
     * 分库密码
     */
    @TableField("DB_PASSWORD")
    @Schema(description = "分库密码")
    private String dbPassword;

    /**
     * 服务截至时间
     */
    @TableField("SERVE_TIME")
    @Schema(description = "服务截至时间")
    private LocalDateTime serveTime;

    /**
     * 租户状态（1启动2停用）
     */
    @TableField("STATUS")
    @Schema(description = "租户状态（1启动2停用）")
    private Integer status;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 备注
     */
    @TableField("REMARK")
    @Schema(description = "备注")
    private String remark;

    /**
     * 是否删除
     */
    @TableField("IS_DELETED")
    @Schema(description = "是否删除")
    private Integer isDeleted;
}
