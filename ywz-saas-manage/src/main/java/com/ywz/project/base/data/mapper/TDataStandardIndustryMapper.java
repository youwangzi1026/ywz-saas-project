package com.ywz.project.base.data.mapper;

import com.ywz.project.base.data.entity.TDataStandardIndustry;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


/**
 * <p>
 * 国标行业表 Mapper 接口
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-28
 */
public interface TDataStandardIndustryMapper extends BaseMapper<TDataStandardIndustry> {

}

