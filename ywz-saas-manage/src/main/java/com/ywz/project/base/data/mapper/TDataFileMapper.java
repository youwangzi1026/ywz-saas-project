package com.ywz.project.base.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ywz.project.base.data.entity.TDataFile;

/**
 * <p>
 * 系统文件 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2025-01-06
 */
public interface TDataFileMapper extends BaseMapper<TDataFile> {

}
