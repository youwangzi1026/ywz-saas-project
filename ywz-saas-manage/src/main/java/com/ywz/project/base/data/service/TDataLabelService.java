package com.ywz.project.base.data.service;

import com.ywz.project.base.data.entity.TDataLabel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 标签表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-11-15
 */
public interface TDataLabelService extends IService<TDataLabel> {

}
