package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysPermissionService extends IService<TSysPermission> {

    /**
     * 方法描述 -> 根据角色id获取权限列表
     *
     * @param roleIds 角色id列表
     * @Author: ywz
     * @Date: 2024/11/05
     */
    List<TSysPermission> getPermissionListByRoleIds(List<Integer> roleIds);

    /**
     * 方法描述 -> 获取权限列表
     *
     * @Author: ywz
     * @Date: 2024/12/22
     */
    List<TSysPermission> getPermissionList();
}
