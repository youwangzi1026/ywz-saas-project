package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysMenuMapper extends BaseMapper<TSysMenu> {

}
