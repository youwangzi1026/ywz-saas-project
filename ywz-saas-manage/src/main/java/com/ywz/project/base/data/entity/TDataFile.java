package com.ywz.project.base.data.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
* <p>
* 系统文件
* </p>
*
* @author 游王子og
* @since 2025-02-21
*/
@Getter
@Setter
@ToString
@TableName("t_data_file")
public class TDataFile implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    @Schema(description = "主键")
    private Integer id;

    /**
     * 文件名
     */
    @TableField("name")
    @Schema(description = "文件名")
    private String name;

    /**
     * 文件类型
     */
    @TableField("type")
    @Schema(description = "文件类型")
    private String type;

    /**
     * 标签id
     */
    @TableField("label_id")
    @Schema(description = "标签id")
    private Integer labelId;

    /**
     * 文件位置
     */
    @TableField("file_url")
    @Schema(description = "文件位置")
    private String fileUrl;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 备注
     */
    @TableField("REMARK")
    @Schema(description = "备注")
    private String remark;

    /**
     * 是否删除
     */
    @TableField("IS_DELETED")
    @Schema(description = "是否删除")
    private Integer isDeleted;
}
