package com.ywz.project.base.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serial;
import io.swagger.v3.oas.annotations.media.Schema;

/**
* <p>
* 操作日志表
* </p>
*
* @author 游王子og
* @since 2025-02-21
*/
@Getter
@Setter
@ToString
@TableName("t_sys_oper_log")
public class TSysOperLog implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 日志主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @Schema(description = "日志主键")
    private Long id;

    /**
     * 模块标题
     */
    @TableField("title")
    @Schema(description = "模块标题")
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    @TableField("business_type")
    @Schema(description = "业务类型（0其它 1新增 2修改 3删除）")
    private Integer businessType;

    /**
     * 方法名称
     */
    @TableField("method")
    @Schema(description = "方法名称")
    private String method;

    /**
     * 请求方式
     */
    @TableField("request_method")
    @Schema(description = "请求方式")
    private String requestMethod;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    @TableField("operator_type")
    @Schema(description = "操作类别（0其它 1后台用户 2手机端用户）")
    private Integer operatorType;

    /**
     * 操作人员
     */
    @TableField("oper_name")
    @Schema(description = "操作人员")
    private String operName;

    /**
     * 部门名称
     */
    @TableField("dept_name")
    @Schema(description = "部门名称")
    private String deptName;

    /**
     * 请求URL
     */
    @TableField("oper_url")
    @Schema(description = "请求URL")
    private String operUrl;

    /**
     * 主机地址
     */
    @TableField("oper_ip")
    @Schema(description = "主机地址")
    private String operIp;

    /**
     * 操作地点
     */
    @TableField("oper_location")
    @Schema(description = "操作地点")
    private String operLocation;

    /**
     * 请求参数
     */
    @TableField("oper_param")
    @Schema(description = "请求参数")
    private String operParam;

    /**
     * 返回参数
     */
    @TableField("json_result")
    @Schema(description = "返回参数")
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    @TableField("status")
    @Schema(description = "操作状态（0正常 1异常）")
    private Integer status;

    /**
     * 错误消息
     */
    @TableField("error_msg")
    @Schema(description = "错误消息")
    private String errorMsg;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 备注
     */
    @TableField("REMARK")
    @Schema(description = "备注")
    private String remark;

    /**
     * 是否删除1是0否
     */
    @TableField("IS_DELETED")
    @Schema(description = "是否删除1是0否")
    private Integer isDeleted;
}
