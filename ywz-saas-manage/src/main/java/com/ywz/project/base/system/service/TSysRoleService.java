package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysRoleService extends IService<TSysRole> {

    /**
     * 方法描述 -> 根据用户id查询角色列表
     *
     * @param userId 用户id
     * @Author: ywz
     * @Date: 2024/11/05
     */
    List<TSysRole> getRoleListByUserId(Integer userId);
}
