package com.ywz.project.base.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ywz.project.base.system.entity.TSysPermission;
import com.ywz.project.base.system.mapper.TSysPermissionMapper;
import com.ywz.project.base.system.service.TSysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
@Service
public class TSysPermissionServiceImpl extends ServiceImpl<TSysPermissionMapper, TSysPermission> implements TSysPermissionService {
    @Resource
    private TSysPermissionMapper tSysPermissionMapper;
    @Override
    public List<TSysPermission> getPermissionListByRoleIds(List<Integer> roleIds) {
        return tSysPermissionMapper.getPermissionListByRoleIds(roleIds);
    }

    @Override
    public List<TSysPermission> getPermissionList() {
        LambdaQueryWrapper<TSysPermission> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TSysPermission::getIsDeleted, 0);
        return list(queryWrapper);
    }
}
