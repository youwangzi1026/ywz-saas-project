package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysRole;
import com.ywz.project.base.system.mapper.TSysRoleMapper;
import com.ywz.project.base.system.service.TSysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
@Service
public class TSysRoleServiceImpl extends ServiceImpl<TSysRoleMapper, TSysRole> implements TSysRoleService {
    @Resource
    private TSysRoleMapper tSysRoleMapper;
    @Override
    public List<TSysRole> getRoleListByUserId(Integer userId) {
        return tSysRoleMapper.getRoleListByUserId(userId);
    }
}
