package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysRolePermission;
import com.ywz.project.base.system.mapper.TSysRolePermissionMapper;
import com.ywz.project.base.system.service.TSysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
@Service
public class TSysRolePermissionServiceImpl extends ServiceImpl<TSysRolePermissionMapper, TSysRolePermission> implements TSysRolePermissionService {

}
