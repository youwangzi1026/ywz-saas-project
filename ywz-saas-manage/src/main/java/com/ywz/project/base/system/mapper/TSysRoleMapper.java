package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysRoleMapper extends BaseMapper<TSysRole> {

    List<TSysRole> getRoleListByUserId(Integer userId);
}
