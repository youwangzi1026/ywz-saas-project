package com.ywz.project.base.data.service.impl;

import com.ywz.project.base.data.entity.TDataNationArea;
import com.ywz.project.base.data.mapper.TDataNationAreaMapper;
import com.ywz.project.base.data.service.ITDataNationAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 国家行政区划信息表 服务实现类
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-28
 */
@Service
public class TDataNationAreaServiceImpl extends ServiceImpl<TDataNationAreaMapper, TDataNationArea> implements ITDataNationAreaService {

}
