package com.ywz.project.base.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 菜单子表
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-21
 */
@Getter
@Setter
@ToString
@TableName("t_sys_menu_sub")
public class TSysMenuSub implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    @Schema(description = "主键")
    private String id;

    /**
     * 菜单名称
     */
    @TableField("name")
    @Schema(description = "菜单名称")
    private String name;

    /**
     * 别名
     */
    @TableField("alias")
    @Schema(description = "别名")
    private String alias;

    /**
     * 父级ID
     */
    @TableField("parent_id")
    @Schema(description = "父级ID")
    private String parentId;

    /**
     * 菜单路径
     */
    @TableField("menu_url")
    @Schema(description = "菜单路径")
    private String menuUrl;

    /**
     * 菜单图标
     */
    @TableField("icon")
    @Schema(description = "菜单图标")
    private String icon;

    /**
     * 菜单图片
     */
    @TableField("img_url")
    @Schema(description = "菜单图片")
    private String imgUrl;

    /**
     * 前台地址
     */
    @TableField("foreground_url")
    @Schema(description = "前台地址")
    private String foregroundUrl;

    /**
     * 是否可见：0否 1是
     */
    @TableField("visible")
    @Schema(description = "是否可见：0否 1是")
    private Integer visible;

    /**
     * 状态 :  0 未启用  1 已启用
     */
    @TableField("status")
    @Schema(description = "状态 :  0 未启用  1 已启用")
    private Integer status;

    /**
     * 排序
     */
    @TableField("sort")
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 是否是父级  0 不是  1是
     */
    @TableField("is_parent")
    @Schema(description = "是否是父级  0 不是  1是")
    private Integer isParent;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 备注
     */
    @TableField("REMARK")
    @Schema(description = "备注")
    private String remark;

    /**
     * 是否删除1是0否
     */
    @TableField("IS_DELETED")
    @Schema(description = "是否删除1是0否")
    private Integer isDeleted;

    /**
     * 子菜单
     */
    @TableField(exist = false)
    @Schema(description = "子菜单")
    private List<TSysMenuSub> children;
}
