package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysMenu;
import com.ywz.project.base.system.mapper.TSysMenuMapper;
import com.ywz.project.base.system.service.TSysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
@Service
public class TSysMenuServiceImpl extends ServiceImpl<TSysMenuMapper, TSysMenu> implements TSysMenuService {

}
