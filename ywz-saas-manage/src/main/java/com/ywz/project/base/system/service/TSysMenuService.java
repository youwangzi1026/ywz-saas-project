package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysMenuService extends IService<TSysMenu> {

}
