package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysDepartment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-11-21
 */
public interface TSysDepartmentService extends IService<TSysDepartment> {

}
