package com.ywz.project.base.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serial;
import io.swagger.v3.oas.annotations.media.Schema;

/**
* <p>
* 角色表
* </p>
*
* @author 游王子og
* @since 2025-02-21
*/
@Getter
@Setter
@ToString
@TableName("t_sys_role")
public class TSysRole implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @Schema(description = "主键")
    private Integer id;

    /**
     * 角色名称
     */
    @TableField("role_name")
    @Schema(description = "角色名称")
    private String roleName;

    /**
     * 角色类型
     */
    @TableField("role_type")
    @Schema(description = "角色类型")
    private String roleType;

    /**
     * 角色描述
     */
    @TableField("role_desc")
    @Schema(description = "角色描述")
    private String roleDesc;

    /**
     * 状态：1:可用；0:不可用
     */
    @TableField("status")
    @Schema(description = "状态：1:可用；0:不可用")
    private Integer status;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 备注
     */
    @TableField("REMARK")
    @Schema(description = "备注")
    private String remark;

    /**
     * 是否删除1是0否
     */
    @TableField("IS_DELETED")
    @Schema(description = "是否删除1是0否")
    private Integer isDeleted;
}
