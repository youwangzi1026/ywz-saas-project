package com.ywz.project.base.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serial;
import io.swagger.v3.oas.annotations.media.Schema;

/**
* <p>
* 部门表
* </p>
*
* @author 游王子og
* @since 2025-02-21
*/
@Getter
@Setter
@ToString
@TableName("t_sys_department")
public class TSysDepartment implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    @Schema(description = "主键")
    private Integer id;

    /**
     * 部门名称
     */
    @TableField("dept_name")
    @Schema(description = "部门名称")
    private String deptName;

    /**
     * 上级部门id
     */
    @TableField("pid")
    @Schema(description = "上级部门id")
    private Integer pid;

    /**
     * 上级部门名称
     */
    @Schema(description = "上级部门名称")
    @TableField(exist = false)
    private String parentDeptName;

    /**
     * 部门层级
     */
    @TableField("level")
    @Schema(description = "部门层级")
    private Integer level;

    /**
     * 部门负责人
     */
    @TableField("dept_head")
    @Schema(description = "部门负责人")
    private String deptHead;

    /**
     * 联系电话
     */
    @TableField("contact_number")
    @Schema(description = "联系电话")
    private String contactNumber;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 备注
     */
    @TableField("REMARK")
    @Schema(description = "备注")
    private String remark;

    /**
     * 是否删除
     */
    @TableField("IS_DELETED")
    @Schema(description = "是否删除")
    private Integer isDeleted;
}
