package com.ywz.project.base.system.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ywz.project.base.system.entity.TSysTenement;
import com.ywz.project.base.system.mapper.TSysTenementMapper;
import com.ywz.project.base.system.service.TSysTenementService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 租户信息表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-11-15
 */
@DS("master")
@Service
public class TSysTenementServiceImpl extends ServiceImpl<TSysTenementMapper, TSysTenement> implements TSysTenementService {

}
