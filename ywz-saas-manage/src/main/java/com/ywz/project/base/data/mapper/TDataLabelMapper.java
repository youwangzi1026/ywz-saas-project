package com.ywz.project.base.data.mapper;

import com.ywz.project.base.data.entity.TDataLabel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标签表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-11-15
 */
public interface TDataLabelMapper extends BaseMapper<TDataLabel> {

}
