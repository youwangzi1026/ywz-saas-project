package com.ywz.project.base.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serial;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

/**
* <p>
* 国家行政区划信息表
* </p>
*
* @author 游王子og
* @since 2025-02-28
*/
@Getter
@Setter
@ToString
@TableName("t_data_nation_area")
public class TDataNationArea implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    @Schema(description = "ID")
    private Integer id;

    /**
     * 父级ID
     */
    @TableField("PARENT_ID")
    @Schema(description = "父级ID")
    private Integer parentId;

    /**
     * 区域代码
     */
    @TableField("CODE")
    @Schema(description = "区域代码")
    private String code;

    /**
     * 区域名称
     */
    @TableField("NAME")
    @Schema(description = "区域名称")
    private String name;

    /**
     * 区域类型;0：国家；1-省级；2：地市级；3：区县级
     */
    @TableField("TYPE")
    @Schema(description = "区域类型;0：国家；1-省级；2：地市级；3：区县级")
    private Integer type;

    /**
     * 经度
     */
    @TableField("LONGITUDE")
    @Schema(description = "经度")
    private String longitude;

    /**
     * 纬度
     */
    @TableField("LATITUDE")
    @Schema(description = "纬度")
    private String latitude;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 子列表
     */
    @TableField(exist = false)
    @Schema(description = "子区域")
    private List<TDataNationArea> children;
}
