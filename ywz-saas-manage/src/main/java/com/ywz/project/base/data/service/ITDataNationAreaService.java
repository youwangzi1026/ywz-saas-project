package com.ywz.project.base.data.service;

import com.ywz.project.base.data.entity.TDataNationArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 国家行政区划信息表 服务类
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-28
 */
public interface ITDataNationAreaService extends IService<TDataNationArea> {

}
