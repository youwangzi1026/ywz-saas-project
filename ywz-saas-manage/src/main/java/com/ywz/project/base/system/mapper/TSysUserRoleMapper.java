package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysUserRoleMapper extends BaseMapper<TSysUserRole> {

}
