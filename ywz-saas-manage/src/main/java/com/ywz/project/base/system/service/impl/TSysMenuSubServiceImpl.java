package com.ywz.project.base.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ywz.project.base.system.entity.TSysMenuSub;
import com.ywz.project.base.system.mapper.TSysMenuSubMapper;
import com.ywz.project.base.system.service.ITSysMenuSubService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单子表 服务实现类
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-18
 */
@Service
public class TSysMenuSubServiceImpl extends ServiceImpl<TSysMenuSubMapper, TSysMenuSub> implements ITSysMenuSubService {

}
