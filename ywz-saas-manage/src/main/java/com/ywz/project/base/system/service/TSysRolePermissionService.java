package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysRolePermissionService extends IService<TSysRolePermission> {

}
