package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysOperLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysOperLogMapper extends BaseMapper<TSysOperLog> {

}
