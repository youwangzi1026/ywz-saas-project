package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysOperLog;
import com.ywz.project.base.system.mapper.TSysOperLogMapper;
import com.ywz.project.base.system.service.TSysOperLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
@Service
public class TSysOperLogServiceImpl extends ServiceImpl<TSysOperLogMapper, TSysOperLog> implements TSysOperLogService {

}
