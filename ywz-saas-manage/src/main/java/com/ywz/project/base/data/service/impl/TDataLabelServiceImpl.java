package com.ywz.project.base.data.service.impl;

import com.ywz.project.base.data.entity.TDataLabel;
import com.ywz.project.base.data.mapper.TDataLabelMapper;
import com.ywz.project.base.data.service.TDataLabelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 标签表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-11-15
 */
@Service
public class TDataLabelServiceImpl extends ServiceImpl<TDataLabelMapper, TDataLabel> implements TDataLabelService {

}
