package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysUserRole;
import com.ywz.project.base.system.mapper.TSysUserRoleMapper;
import com.ywz.project.base.system.service.TSysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
@Service
public class TSysUserRoleServiceImpl extends ServiceImpl<TSysUserRoleMapper, TSysUserRole> implements TSysUserRoleService {

}
