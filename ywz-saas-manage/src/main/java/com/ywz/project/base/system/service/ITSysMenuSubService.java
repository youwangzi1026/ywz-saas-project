package com.ywz.project.base.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ywz.project.base.system.entity.TSysMenuSub;

/**
 * <p>
 * 菜单子表 服务类
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-18
 */
public interface ITSysMenuSubService extends IService<TSysMenuSub> {

}
