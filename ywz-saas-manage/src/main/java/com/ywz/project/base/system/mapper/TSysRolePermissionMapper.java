package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysRolePermissionMapper extends BaseMapper<TSysRolePermission> {

}
