package com.ywz.project.base.data.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ywz.project.base.data.entity.TDataFile;
import com.ywz.project.base.data.mapper.TDataFileMapper;
import com.ywz.project.base.data.service.TDataFileService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统文件 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2025-01-06
 */
@Service
public class TDataFileServiceImpl extends ServiceImpl<TDataFileMapper, TDataFile> implements TDataFileService {

}
