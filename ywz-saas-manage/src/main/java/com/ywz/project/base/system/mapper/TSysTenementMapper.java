package com.ywz.project.base.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ywz.project.base.system.entity.TSysTenement;

/**
 * <p>
 * 租户信息表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-11-15
 */
public interface TSysTenementMapper extends BaseMapper<TSysTenement> {

}
