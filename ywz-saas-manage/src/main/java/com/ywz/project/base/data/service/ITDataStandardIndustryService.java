package com.ywz.project.base.data.service;

import com.ywz.project.base.data.entity.TDataStandardIndustry;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 国标行业表 服务类
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-28
 */
public interface ITDataStandardIndustryService extends IService<TDataStandardIndustry> {

}
