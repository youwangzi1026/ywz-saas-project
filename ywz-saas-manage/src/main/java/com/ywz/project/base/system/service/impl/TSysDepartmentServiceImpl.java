package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysDepartment;
import com.ywz.project.base.system.mapper.TSysDepartmentMapper;
import com.ywz.project.base.system.service.TSysDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-11-21
 */
@Service
public class TSysDepartmentServiceImpl extends ServiceImpl<TSysDepartmentMapper, TSysDepartment> implements TSysDepartmentService {

}
