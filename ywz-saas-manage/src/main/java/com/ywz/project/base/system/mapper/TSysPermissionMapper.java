package com.ywz.project.base.system.mapper;

import com.ywz.project.base.system.entity.TSysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysPermissionMapper extends BaseMapper<TSysPermission> {

    List<TSysPermission> getPermissionListByRoleIds(List<Integer> roleIds);
}
