package com.ywz.project.base.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serial;
import io.swagger.v3.oas.annotations.media.Schema;

/**
* <p>
* 权限表
* </p>
*
* @author 游王子og
* @since 2025-02-21
*/
@Getter
@Setter
@ToString
@TableName("t_sys_permission")
public class TSysPermission implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @Schema(description = "主键")
    private Integer id;

    /**
     * 所属菜单，父子菜单使用符号进行拼接
     */
    @TableField("menu_names")
    @Schema(description = "所属菜单，父子菜单使用符号进行拼接")
    private String menuNames;

    /**
     * 菜单子表
     */
    @TableField("menu_sub_names")
    @Schema(description = "菜单子表")
    private String menuSubNames;

    /**
     * 权限名
     */
    @TableField("permission_name")
    @Schema(description = "权限名")
    private String permissionName;

    /**
     * 权限中文描述
     */
    @TableField("permission_desc")
    @Schema(description = "权限中文描述")
    private String permissionDesc;

    /**
     * 分组id
     */
    @TableField("pid")
    @Schema(description = "分组id")
    private Integer pid;

    /**
     * 是否是父级  1 父 0 子  2 操作
     */
    @TableField("is_parent")
    @Schema(description = "是否是父级  1 父 0 子  2 操作")
    private Integer isParent;

    /**
     * 1增加 2 查看 3修改  4 删除 5状态  6权限
     */
    @TableField("is_operating")
    @Schema(description = "1增加 2 查看 3修改  4 删除 5状态  6权限")
    private Integer isOperating;

    /**
     * 排序
     */
    @TableField("sort")
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 备注
     */
    @TableField("REMARK")
    @Schema(description = "备注")
    private String remark;

    /**
     * 是否删除1是0否
     */
    @TableField("IS_DELETED")
    @Schema(description = "是否删除1是0否")
    private Integer isDeleted;
}
