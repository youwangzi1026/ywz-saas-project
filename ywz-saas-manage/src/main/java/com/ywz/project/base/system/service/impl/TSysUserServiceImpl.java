package com.ywz.project.base.system.service.impl;

import com.ywz.project.base.system.entity.TSysUser;
import com.ywz.project.base.system.mapper.TSysUserMapper;
import com.ywz.project.base.system.service.TSysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ywz
 * @since 2024-11-14
 */
@Service
public class TSysUserServiceImpl extends ServiceImpl<TSysUserMapper, TSysUser> implements TSysUserService {

}
