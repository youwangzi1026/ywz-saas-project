package com.ywz.project.base.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serial;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

/**
* <p>
* 国标行业表
* </p>
*
* @author 游王子og
* @since 2025-02-28
*/
@Getter
@Setter
@ToString
@TableName("t_data_standard_industry")
public class TDataStandardIndustry implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    @Schema(description = "ID")
    private Integer id;

    /**
     * 父级行业代码
     */
    @TableField("PARENT_CODE")
    @Schema(description = "父级行业代码")
    private String parentCode;

    /**
     * 行业代码
     */
    @TableField("CODE")
    @Schema(description = "行业代码")
    private String code;

    /**
     * 行业名称
     */
    @TableField("NAME")
    @Schema(description = "行业名称")
    private String name;

    /**
     * 状态;0：未启用；1：启用
     */
    @TableField("STATUS")
    @Schema(description = "状态;0：未启用；1：启用")
    private Integer status;

    /**
     * 乐观锁
     */
    @TableField("REVISION")
    @Schema(description = "乐观锁")
    @Version
    private Integer revision;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    @Schema(description = "创建人")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("CREATED_TIME")
    @Schema(description = "创建时间")
    private LocalDateTime createdTime;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    @Schema(description = "更新人")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("UPDATED_TIME")
    @Schema(description = "更新时间")
    private LocalDateTime updatedTime;

    /**
     * 子节点
     */
    @TableField(exist = false)
    @Schema(description = "子行业")
    private List<TDataStandardIndustry> children;
}
