package com.ywz.project.base.system.service;

import com.ywz.project.base.system.entity.TSysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author ywz
 * @since 2024-10-27
 */
public interface TSysUserRoleService extends IService<TSysUserRole> {

}
