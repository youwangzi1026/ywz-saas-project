package com.ywz.project.base.data.service.impl;

import com.ywz.project.base.data.entity.TDataStandardIndustry;
import com.ywz.project.base.data.mapper.TDataStandardIndustryMapper;
import com.ywz.project.base.data.service.ITDataStandardIndustryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 国标行业表 服务实现类
 * </p>
 *
 * @author 游王子og
 * @since 2025-02-28
 */
@Service
public class TDataStandardIndustryServiceImpl extends ServiceImpl<TDataStandardIndustryMapper, TDataStandardIndustry> implements ITDataStandardIndustryService {

}
