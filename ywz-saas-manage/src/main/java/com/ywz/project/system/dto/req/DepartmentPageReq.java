package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 部门分页请求
 *
 * @Author: ywz
 * @Date: 2024/11/22
 */
@Data
@Schema(description = "部门分页请求")
public class DepartmentPageReq {
    @Schema(description = "部门名称")
    private String deptName;
    @Schema(description = "部门负责人")
    private String deptHead;
    @Schema(description = "页码")
    private int pageNum;
    @Schema(description = "每页条数")
    private int pageSize;
}
