package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditRoleReq;
import com.ywz.project.system.dto.req.RolePageReq;
import com.ywz.project.system.service.RoleApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 类描述 -> 角色API
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Tag(name = "角色管理")
@RestController
@RequestMapping("/system/role")
public class RoleApiController {
    @Resource
    private RoleApiService sysRoleApiService;

    /**
     * 方法描述 -> 获取角色分页
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @PreAuthorize("hasAuthority('PERM_ROLE_PAGE')")
    @GetMapping("/getRolePage")
    @Operation(summary = "获取角色分页")
    public ResultResp getRolePage(RolePageReq req) {
        return sysRoleApiService.getRolePage(req);
    }

    /**
     * 方法描述 -> 获取角色列表
     *
     * @Author: ywz
     * @Date: 2024/11/22
     */
    @PreAuthorize("hasAuthority('PERM_ROLE_LIST')")
    @GetMapping("/getRoleList")
    @Operation(summary = "获取角色列表")
    public ResultResp getRoleList() {
        return sysRoleApiService.getRoleList();
    }

    /**
     * 方法描述 -> 根据用户ID获取角色列表
     *
     * @param userId 用户ID
     * @Author: ywz
     * @Date: 2024/11/08
     */
    @PreAuthorize("hasAuthority('PERM_ROLE_LIST_BY_USER_ID')")
    @GetMapping("/getRoleListByUserId")
    @Operation(summary = "根据用户ID获取角色列表")
    public ResultResp getRoleListByUserId(Integer userId) {
        return sysRoleApiService.getRoleListByUserId(userId);
    }

    /**
     * 方法描述 -> 新增编辑角色
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/21
     */
    @PreAuthorize("hasAuthority('PERM_ROLE_EDIT')")
    @PostMapping("/editRole")
    @Operation(summary = "新增编辑角色")
    public ResultResp editRole(EditRoleReq req) {
        return sysRoleApiService.editRole(req);
    }

    /**
     * 方法描述 -> 删除角色
     *
     * @param ids 角色ID
     * @Author: ywz
     * @Date: 2024/11/21
     */
    @PreAuthorize("hasAuthority('PERM_ROLE_DELETE')")
    @DeleteMapping("/removeRole")
    @Operation(summary = "删除角色")
    public ResultResp removeRole(String ids) {
        return sysRoleApiService.removeRole(ids);
    }
}
