package com.ywz.project.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ywz.common.ResultResp;
import com.ywz.common.StringUtils;
import com.ywz.project.base.system.entity.TSysPermission;
import com.ywz.project.base.system.service.TSysPermissionService;
import com.ywz.project.system.dto.req.EditPermissionReq;
import com.ywz.project.system.dto.req.PermissionPageReq;
import com.ywz.project.system.service.PermissionApiService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PermissionApiServiceImpl implements PermissionApiService {
    @Resource
    private TSysPermissionService tSysPermissionService;

    @Override
    public ResultResp getPermissionPage(PermissionPageReq req) {
        LambdaQueryWrapper<TSysPermission> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TSysPermission::getIsDeleted, 0);
        if (!StringUtils.isEmpty(req.getPermissionDesc()))
            wrapper.like(TSysPermission::getPermissionDesc, req.getPermissionDesc());
        if (!StringUtils.isEmpty(req.getMenuNames()))
            wrapper.like(TSysPermission::getMenuNames, req.getMenuNames());
        wrapper.orderByDesc(TSysPermission::getUpdatedTime);
        Page<TSysPermission> permissionPage = tSysPermissionService.page(new Page<>(req.getPageNum(), req.getPageSize()), wrapper);
        return ResultResp.success(permissionPage);
    }

    @Override
    public ResultResp getPermissionListByRoleId(String roleIds) {
        if (!StringUtils.isNumberAndSeparator(roleIds))
            return ResultResp.error("角色ids拼接格式错误");
        String[] split = roleIds.split(StringUtils.SEPARATOR);
        List<Integer> roleIdsList = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
        List<TSysPermission> permissionListByRoleIds = tSysPermissionService.getPermissionListByRoleIds(roleIdsList);
        return ResultResp.success(permissionListByRoleIds);
    }

    @Override
    public ResultResp editPermission(EditPermissionReq req) {
        // 参数校验
        if (StringUtils.isEmpties(req.getPermissionName(), req.getMenuNames(), req.getPermissionDesc()))
            return ResultResp.error("参数不能为空");
        if (StringUtils.isNull(req.getIsParent()))
            return ResultResp.error("isParent不能为空");
        if (StringUtils.isOverLength(255, req.getPermissionDesc(), req.getRemark(), req.getPermissionName(), req.getMenuNames()))
            return ResultResp.error("参数长度超出限制");

        TSysPermission tSysPermission = new TSysPermission();
        BeanUtils.copyProperties(req, tSysPermission);
        tSysPermission.setUpdatedTime(LocalDateTime.now());
        boolean saved = tSysPermissionService.saveOrUpdate(tSysPermission);
        return saved ? ResultResp.success() : ResultResp.error();
    }

    @Override
    public ResultResp removePermission(String ids) {
        if (StringUtils.isNumberAndSeparator(ids)) {
            String[] split = ids.split(StringUtils.SEPARATOR);
            List<Integer> idsList = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
            LambdaUpdateWrapper<TSysPermission> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.in(TSysPermission::getId, idsList).set(TSysPermission::getIsDeleted, 1);
            updateWrapper.set(TSysPermission::getUpdatedTime, LocalDateTime.now());
            boolean saved = tSysPermissionService.update(updateWrapper);
            if (saved)
                return ResultResp.success();
            return ResultResp.error("删除失败");
        } else {
            return ResultResp.error("ids拼接格式错误");
        }
    }

    @Override
    public ResultResp getPermissionList() {
        LambdaQueryWrapper<TSysPermission> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TSysPermission::getIsDeleted, 0);
        return ResultResp.success(tSysPermissionService.list(queryWrapper));
    }
}
