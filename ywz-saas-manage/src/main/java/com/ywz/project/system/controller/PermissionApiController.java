package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditPermissionReq;
import com.ywz.project.system.dto.req.PermissionPageReq;
import com.ywz.project.system.service.PermissionApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 类描述 -> 权限管理API
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Tag(name = "权限管理")
@RestController
@RequestMapping("/system/permission")
public class PermissionApiController {
    @Resource
    private PermissionApiService sysPermissionApiService;

    /**
     * 方法描述 -> 获取权限分页
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @PreAuthorize("hasAuthority('PERM_PERM_PAGE')")
    @GetMapping("/getPermissionPage")
    @Operation(summary = "获取权限分页")
    public ResultResp getPermissionPage(PermissionPageReq req) {
        return sysPermissionApiService.getPermissionPage(req);
    }

    /**
     * 方法描述 -> 获取权限集合
     *
     * @Author: ywz
     * @Date: 2024/11/21
     */
    @PreAuthorize("hasAuthority('PERM_PERM_LIST')")
    @GetMapping("/getPermissionList")
    @Operation(summary = "获取权限集合")
    public ResultResp getPermissionList(){
        return sysPermissionApiService.getPermissionList();
    }

    /**
     * 方法描述 -> 根据角色id集合获取权限集合
     *
     * @param roleIds 角色id集合
     * @Author: ywz
     * @Date: 2024/11/08
     */
    @PreAuthorize("hasAuthority('PERM_PERM_LIST_BY_ROLE_ID')")
    @GetMapping("/getPermissionListByRoleId")
    @Operation(summary = "根据角色id集合获取权限集合")
    public ResultResp getPermissionListByRoleId(String roleIds) {
        return sysPermissionApiService.getPermissionListByRoleId(roleIds);
    }

    /**
     * 方法描述 -> 新增编辑权限
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/21
     */
    @PreAuthorize("hasAuthority('PERM_PERM_EDIT')")
    @PostMapping("/editPermission")
    @Operation(summary = "编辑权限")
    public ResultResp editPermission(EditPermissionReq req) {
        return sysPermissionApiService.editPermission(req);
    }

    /**
     * 方法描述 -> 删除权限
     *
     * @param ids 权限id集合
     * @Author: ywz
     * @Date: 2024/11/21
     */
    @PreAuthorize("hasAuthority('PERM_PERM_DELETE')")
    @DeleteMapping("/removePermission")
    @Operation(summary = "删除权限")
    public ResultResp removePermission(String ids) {
        return sysPermissionApiService.removePermission(ids);
    }
}
