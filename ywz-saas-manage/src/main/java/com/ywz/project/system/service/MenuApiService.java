package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.MenuPageReq;

/**
 * 类描述 -> 系统菜单业务
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
public interface MenuApiService {
    /**
     * 方法描述 -> 获取菜单分页数据
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    ResultResp getMenuPage(MenuPageReq req);

    /**
     * 方法描述 -> 获取菜单树
     *
     * @Author: ywz
     * @Date: 2024/11/13
     */
    ResultResp getMenuTree();

    /**
     * 方法描述 -> 保存菜单
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/19
     */
    ResultResp saveMenu(String req);

    /**
     * 方法描述 -> 获取租户菜单列表
     *
     * @param key 租户key
     * @Author: ywz
     * @Date: 2024/11/20
     */
    ResultResp getTenementMenuTree(String key);

    /**
     * 方法描述 -> 获取当前用户菜单树
     *
     * @Author: ywz
     * @Date: 2024/11/20
     */
    ResultResp getCurrentMenuTree();
}
