package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 新增、修改租户参数
 *
 * @Author: ywz
 * @Date: 2024/11/16
 */
@Data
@Schema(description = "新增、修改租户参数")
public class EditTenementReq {
    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "租户名")
    private String name;

    @Schema(description = "租户简称")
    private String simpleName;

    @Schema(description = "分库标识")
    private String tenantKey;

    @Schema(description = "分库地址")
    private String dbUrl;

    @Schema(description = "服务截至时间")
    private String serveTime;

    @Schema(description = "租户状态（1启动2停用）")
    private Integer status;

}
