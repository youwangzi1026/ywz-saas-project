package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.OperLogPageReq;
import com.ywz.project.system.service.OperLogApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 类描述 -> 操作日志API
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Tag(name = "操作日志")
@RestController
@RequestMapping("/system/operLog")
public class OperLogApiController {
    @Resource
    private OperLogApiService sysOperLogApiService;

    /**
     * 方法描述 -> 分页查询操作日志
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @PreAuthorize("hasAuthority('PERM_OPERLOG_PAGE')")
    @GetMapping("/getOperLogPage")
    @Operation(summary = "分页查询操作日志")
    public ResultResp getOperLogPage(OperLogPageReq req) {
        return sysOperLogApiService.getOperLogPage(req);
    }

    /**
     * 方法描述 -> 删除操作日志
     *
     * @param ids 操作日志id
     * @Author: ywz
     * @Date: 2024/12/22
     */
    @PreAuthorize("hasAuthority('PERM_OPERLOG_DELETE')")
    @DeleteMapping("/removeOperLog")
    @Operation(summary = "删除操作日志")
    public ResultResp removeOperLog(String ids) {
        return sysOperLogApiService.removeOperLog(ids);
    }
}
