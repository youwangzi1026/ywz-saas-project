package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 登录日志分页请求
 *
 * @Author: ywz
 * @Date: 2024/11/05
 */
@Data
@Schema(description = "登录日志分页请求")
public class LoginLogPageReq {

    @Schema(description = "页码")
    private int pageNum;

    @Schema(description = "每页大小")
    private int pageSize;

    @Schema(description = "用户名")
    private String user;

    @Schema(description = "开始时间")
    private String startTime;

    @Schema(description = "结束时间")
    private String endTime;
}
