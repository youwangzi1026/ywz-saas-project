package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 添加编辑部门请求参数
 *
 * @Author: ywz
 * @Date: 2024/11/28
 */
@Data
@Schema(description = "添加编辑部门请求参数")
public class EditDepartmentReq {
    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "部门名称")
    private String deptName;

    @Schema(description = "上级部门id")
    private Integer pid;

    @Schema(description = "上级部门名称")
    private String parentDeptName;

    @Schema(description = "部门层级")
    private Integer level;

    @Schema(description = "部门负责人")
    private String deptHead;

    @Schema(description = "联系电话")
    private String contactNumber;

    @Schema(description = "备注")
    private String remark;

}
