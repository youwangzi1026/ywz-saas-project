package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.MenuPageReq;
import com.ywz.project.system.service.MenuApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.annotation.security.PermitAll;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类描述 -> 系统菜单API
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Tag(name = "系统菜单")
@RestController
@RequestMapping("/system/menu")
public class MenuApiController {
    @Resource
    private MenuApiService sysMenuApiService;

    /**
     * 方法描述 -> 获取菜单分页数据
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @PreAuthorize("hasAuthority('PERM_MENU_PAGE')")
    @GetMapping("/getMenuPage")
    @Operation(summary = "获取菜单分页数据")
    public ResultResp getMenuPage(MenuPageReq req) {
        return sysMenuApiService.getMenuPage(req);
    }

    /**
     * 方法描述 -> 获取菜单树
     *
     * @Author: ywz
     * @Date: 2024/11/13
     */
    @PreAuthorize("hasAuthority('PERM_MENU_TREE')")
    @GetMapping("/getMenuTree")
    @Operation(summary = "获取菜单树")
    public ResultResp getMenuTree() {
        return sysMenuApiService.getMenuTree();
    }

    /**
     * 方法描述 -> 获取当前用户菜单树
     *
     * @Author: ywz
     * @Date: 2024/12/22
     */
    @PermitAll
    @GetMapping("/getCurrentMenuTree")
    @Operation(summary = "获取当前用户菜单树")
    public ResultResp getCurrentMenuTree() {
        return sysMenuApiService.getCurrentMenuTree();
    }

    /**
     * 方法描述 -> 获取租户菜单列表
     *
     * @param key 租户key
     * @Author: ywz
     * @Date: 2024/11/20
     */
    @PreAuthorize("hasAuthority('PERM_MENU_TENEMENT_TREE')")
    @GetMapping("/getTenementMenuTree")
    @Operation(summary = "获取租户菜单树")
    public ResultResp getTenementMenuTree(String key) {
        return sysMenuApiService.getTenementMenuTree(key);
    }

    /**
     * 方法描述 -> 保存菜单
     *
     * @param json 菜单json
     * @Author: ywz
     * @Date: 2024/12/22
     */
    @PreAuthorize("hasAuthority('PERM_MENU_SAVE')")
    @PostMapping("/saveMenu")
    @Operation(summary = "保存菜单")
    public ResultResp saveMenu(String json) {
        return sysMenuApiService.saveMenu(json);
    }
}
