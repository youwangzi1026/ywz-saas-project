package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditRoleReq;
import com.ywz.project.system.dto.req.RolePageReq;

/**
 * 类描述 -> 系统角色业务
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
public interface RoleApiService {

    /**
     * 方法描述 -> 分页查询角色
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    ResultResp getRolePage(RolePageReq req);

    /**
     * 方法描述 -> 根据用户ID获取角色列表
     *
     * @param userId 用户ID
     * @Author: ywz
     * @Date: 2024/11/08
     */
    ResultResp getRoleListByUserId(Integer userId);

    /**
     * 方法描述 -> 新增角色
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/21
     */
    ResultResp editRole(EditRoleReq req);

    /**
     * 方法描述 -> 删除角色
     *
     * @param ids 角色ID
     * @Author: ywz
     * @Date: 2024/11/21
     */
    ResultResp removeRole(String ids);

    /**
     * 方法描述 -> 获取所有角色
     *
     * @Author: ywz
     * @Date: 2024/11/22
     */
    ResultResp getRoleList();
}
