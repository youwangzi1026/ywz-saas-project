package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.LoginReq;
import com.ywz.project.system.dto.req.RegisterReq;

/**
 * 类描述 -> 登录业务
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
public interface LoginService {
    /**
     * 方法描述 -> 登录
     *
     * @param req 登录参数
     * @Author: ywz
     * @Date: 2024/10/27
     */
    ResultResp login(LoginReq req);

    /**
     * 方法描述 -> 注册
     *
     * @param req 注册参数
     * @Author: ywz
     * @Date: 2024/10/27
     */
    ResultResp register(RegisterReq req);

    /**
     * 方法描述 -> 登出
     *
     *
     * @Author: ywz
     * @Date: 2024/10/27
     */
    ResultResp logout();

    /**
     * 方法描述 -> 修改密码
     *
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @Author: ywz
     * @Date: 2024/10/27
     */
    ResultResp resetPassword(String oldPassword, String newPassword);
}
