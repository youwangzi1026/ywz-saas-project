package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditUserReq;
import com.ywz.project.system.dto.req.UserPageReq;

/**
 * 类描述 -> 系统用户
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
public interface UserApiService {
    /**
     * 方法描述 -> 分页查询用户
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    ResultResp getUserPage(UserPageReq req);

    /**
     * 方法描述 -> 编辑用户
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/23
     */
    ResultResp editUser(EditUserReq req);

    /**
     * 方法描述 -> 删除用户
     *
     * @param ids 用户id
     * @Author: ywz
     * @Date: 2024/11/23
     */
    ResultResp removeUser(String ids);
}
