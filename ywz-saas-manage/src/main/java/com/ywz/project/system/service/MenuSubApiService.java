package com.ywz.project.system.service;

import com.ywz.common.ResultResp;

/**
 * 类描述 -> 菜单子表服务接口
 *
 * @Author: ywz
 * @Date: 2025/02/18
 */
public interface MenuSubApiService {
    /**
     * 方法描述 -> 根据菜单关联标识获取菜单子表列表
     *
     * @param remark 关联标识
     * @Author: ywz
     * @Date: 2025/02/18
     */
    ResultResp getMenuSubListByRemark(String remark);

    /**
     * 方法描述 -> 保存菜单子表
     *
     * @param json json字符串
     * @Author: ywz
     * @Date: 2025/02/18
     */
    ResultResp saveMenuSub(String json);
}
