package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.LoginLogPageReq;

/**
 * 类描述 -> 系统登录日志业务
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
public interface LoginLogApiService {
    /**
     * 方法描述 -> 分页查询系统登录日志
     *
     * @param req -> 分页请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    ResultResp getLoginLogPage(LoginLogPageReq req);

    /**
     * 方法描述 -> 删除系统登录日志
     *
     * @param ids -> 登录日志ID
     * @Author: ywz
     * @Date: 2024/11/24
     */
    ResultResp removeLoginLog(String ids);
}
