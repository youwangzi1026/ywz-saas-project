package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.LoginLogPageReq;
import com.ywz.project.system.service.LoginLogApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 类描述 -> 系统登录日志API
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Tag(name = "系统登录日志")
@RestController
@RequestMapping("/system/sysLoginLog")
public class LoginLogApiController {
    @Resource
    private LoginLogApiService sysLoginLogApiService;

    /**
     * 方法描述 -> 分页查询系统登录日志
     *
     * @param req -> 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @PreAuthorize("hasAuthority('PERM_LOGINLOG_PAGE')")
    @GetMapping("/getLoginLogPage")
    @Operation(summary = "分页查询系统登录日志")
    public ResultResp getLoginLogPage(LoginLogPageReq req) {
        return sysLoginLogApiService.getLoginLogPage(req);
    }

    /**
     * 方法描述 -> 删除系统登录日志
     *
     * @param ids -> 日志ID
     * @Author: ywz
     * @Date: 2024/11/24
     */
    @PreAuthorize("hasAuthority('PERM_LOGINLOG_DELETE')")
    @DeleteMapping("/removeLoginLog")
    @Operation(summary = "删除系统登录日志")
    public ResultResp removeLoginLog(String ids) {
        return sysLoginLogApiService.removeLoginLog(ids);
    }

}
