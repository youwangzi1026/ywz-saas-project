package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 菜单分页请求
 *
 * @Author: ywz
 * @Date: 2024/11/05
 */
@Data
@Schema(description = "菜单分页请求")
public class MenuPageReq {
    @Schema(description = "页码")
    private int pageNum;
    @Schema(description = "每页大小")
    private int pageSize;
}
