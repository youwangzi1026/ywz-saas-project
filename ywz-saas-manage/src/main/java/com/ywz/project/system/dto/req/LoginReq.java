package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 登录请求参数
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Data
@Schema(description = "登录请求参数")
public class LoginReq {
    @Schema(description = "用户名")
    private String username;
    @Schema(description = "密码")
    private String password;
    @Schema(description = "验证码key")
    private String captchaKey;
    @Schema(description = "验证码")
    private String captchaCode;
}
