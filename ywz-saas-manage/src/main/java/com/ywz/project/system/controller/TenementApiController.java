package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditTenementReq;
import com.ywz.project.system.dto.req.TenementPageReq;
import com.ywz.project.system.service.TenementApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 类描述 -> 租户API接口
 *
 * @Author: ywz
 * @Date: 2024/11/14
 */
@Tag(name = "租户管理")
@RestController
@RequestMapping("/tenement")
public class TenementApiController {
    @Resource
    private TenementApiService tenementApiService;

    /**
     * 方法描述 -> 获取租户信息
     *
     * @Author: ywz
     * @Date: 2024/11/14
     */
    @PreAuthorize("hasAuthority('PERM_TENEMENT_LIST')")
    @GetMapping("/getTenementList")
    @Operation(summary = "获取租户信息")
    public ResultResp getTenementList() {
        return tenementApiService.getTenementList();
    }

    /**
     * 方法描述 -> 获取租户分页信息
     *
     * @Author: ywz
     * @Date: 2024/11/15
     */
    @PreAuthorize("hasAuthority('PERM_TENEMENT_PAGE')")
    @GetMapping("/getTenementPage")
    @Operation(summary = "获取租户分页信息")
    public ResultResp getTenementPage(TenementPageReq req) {
        return tenementApiService.getTenementPage(req);
    }

    /**
     * 方法描述 -> 新增、编辑租户信息
     *
     * @param req 新增租户请求参数
     * @Author: ywz
     * @Date: 2024/11/16
     */
    @PreAuthorize("hasAuthority('PERM_TENEMENT_EDIT')")
    @PostMapping("/editTenement")
    @Operation(summary = "新增、编辑租户信息")
    public ResultResp editTenement(EditTenementReq req) {
        return tenementApiService.editTenement(req);
    }

    /**
     * 方法描述 -> 删除租户信息
     *
     * @param ids 租户id
     * @Author: ywz
     * @Date: 2024/11/19
     */
    @PreAuthorize("hasAuthority('PERM_TENEMENT_DELETE')")
    @DeleteMapping("/removeTenement")
    @Operation(summary = "删除租户信息")
    public ResultResp removeTenement(String ids) {
        return tenementApiService.removeTenement(ids);
    }
}
