package com.ywz.project.system.dto.resp;

import com.ywz.project.base.system.entity.TSysRole;
import com.ywz.project.base.system.entity.TSysUser;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 类描述 -> 用户分页返回
 *
 * @Author: ywz
 * @Date: 2024/11/22
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "用户分页返回")
public class UserPageResp extends TSysUser {
    @Schema(description = "部门名称")
    private String deptName;

    @Schema(description = "角色名称")
    private String roleName;

    @Schema(description = "角色列表")
    private List<TSysRole> roles;
}
