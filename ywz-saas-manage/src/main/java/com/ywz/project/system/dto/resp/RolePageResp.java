package com.ywz.project.system.dto.resp;

import com.ywz.project.base.system.entity.TSysPermission;
import com.ywz.project.base.system.entity.TSysRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 类描述 -> 角色分页响应对象
 *
 * @Author: ywz
 * @Date: 2024/11/21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "角色分页响应对象")
public class RolePageResp extends TSysRole {

    @Schema(description = "权限列表")
    private List<TSysPermission> permissions;

    @Schema(description = "权限名称")
    private String permissionName;
}
