package com.ywz.project.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ywz.common.ResultResp;
import com.ywz.common.StringUtils;
import com.ywz.project.base.system.entity.TSysDepartment;
import com.ywz.project.base.system.service.TSysDepartmentService;
import com.ywz.project.system.dto.req.DepartmentPageReq;
import com.ywz.project.system.dto.req.EditDepartmentReq;
import com.ywz.project.system.service.DepartmentApiService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentApiServiceImpl implements DepartmentApiService {

    @Resource
    private TSysDepartmentService tDepartmentService;

    @Override
    public ResultResp getDepartmentPage(DepartmentPageReq req) {
        // 查询条件：部门、部门负责人
        LambdaQueryWrapper<TSysDepartment> wrapper = new LambdaQueryWrapper<>();
        if (!StringUtils.isEmpty(req.getDeptName()))
            wrapper.like(TSysDepartment::getDeptName, req.getDeptName());
        if (!StringUtils.isEmpty(req.getDeptHead()))
            wrapper.like(TSysDepartment::getDeptHead, req.getDeptHead());
        wrapper.eq(TSysDepartment::getIsDeleted, 0);
        // 分页查询
        Page<TSysDepartment> page = tDepartmentService.page(new Page<>(req.getPageNum(), req.getPageSize()), wrapper);
        List<TSysDepartment> records = page.getRecords();
        page.getRecords().forEach(tDepartment -> {
            if (!StringUtils.isNull(tDepartment.getPid())) {
                records.forEach(tDepartment1 -> {
                    if (tDepartment.getPid().equals(tDepartment1.getId()))
                        tDepartment.setParentDeptName(tDepartment1.getDeptName());
                });
            }
        });
        return ResultResp.success(page);
    }

    @Override
    public ResultResp getDepartmentList() {
        LambdaQueryWrapper<TSysDepartment> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(TSysDepartment::getIsDeleted, 0);
        return ResultResp.success(tDepartmentService.list(wrapper));
    }

    @Override
    public ResultResp editDepartment(EditDepartmentReq req) {
        if (StringUtils.isEmpties(req.getDeptName(), req.getDeptHead(), req.getContactNumber()))
            return ResultResp.error("参数为空");
        if (StringUtils.isOverLength(255, req.getDeptName(), req.getDeptHead()
                , req.getContactNumber(), req.getRemark()))
            return ResultResp.error("参数长度过长");
        // 新增或修改
        TSysDepartment department = new TSysDepartment();
        BeanUtils.copyProperties(req, department);
        boolean saved = tDepartmentService.saveOrUpdate(department);
        return saved? ResultResp.success() : ResultResp.error();
    }

    @Override
    public ResultResp removeDepartment(String ids) {
        if (StringUtils.isNumberAndSeparator(ids)) {
            String[] split = ids.split(StringUtils.SEPARATOR);
            List<Integer> collect = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
            LambdaUpdateWrapper<TSysDepartment> wrapper = new LambdaUpdateWrapper<>();
            // 去掉关联部门的pid
            wrapper.in(TSysDepartment::getPid, collect);
            wrapper.set(TSysDepartment::getPid, null);
            tDepartmentService.update(wrapper);
            // 删除部门
            wrapper.clear(); // 重置条件
            wrapper.in(TSysDepartment::getId, collect);
            wrapper.set(TSysDepartment::getIsDeleted, 1);
            wrapper.set(TSysDepartment::getUpdatedTime, LocalDateTime.now());
            boolean update = tDepartmentService.update(wrapper);
            if (update)
                return ResultResp.success();
            return ResultResp.error("删除失败");
        }
        return ResultResp.error("参数格式错误");
    }
}
