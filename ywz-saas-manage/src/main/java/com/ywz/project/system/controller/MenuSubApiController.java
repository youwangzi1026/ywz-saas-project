package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.service.MenuSubApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类描述 -> 菜单子表
 *
 * @Author: ywz
 * @Date: 2025/02/18
 */
@Tag(name = "菜单子表")
@RestController
@RequestMapping("/menuSub")
public class MenuSubApiController {
    @Resource
    private MenuSubApiService menuSubApiService;

    /**
     * 方法描述 -> 根据菜单关联标识获取菜单子表列表
     *
     * @param remark 关联标识
     * @Author: ywz
     * @Date: 2025/02/18
     */
    @GetMapping("/getMenuSubListByRemark")
    @Operation(summary = "根据菜单id获取菜单子表列表")
    public ResultResp getMenuSubListByRemark(String remark) {
        return menuSubApiService.getMenuSubListByRemark(remark);
    }

    /**
     * 方法描述 -> 保存菜单子表
     *
     * @param json json字符串
     * @Author: ywz
     * @Date: 2025/02/18
     */
    @PostMapping("/saveMenuSub")
    @Operation(summary = "保存菜单子表")
    public ResultResp saveMenuSub(String json){
        return menuSubApiService.saveMenuSub(json);
    }
}
