package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 分页查询租户参数
 *
 * @Author: ywz
 * @Date: 2024/11/15
 */
@Schema(description = "分页查询租户参数")
@Data
public class TenementPageReq {
    @Schema(description = "租户名称")
    private String name;
    @Schema(description = "租户简称")
    private String simpleName;
    @Schema(description = "开始时间")
    private String startDate;
    @Schema(description = "结束时间")
    private String endDate;
    @Schema(description = "当前页")
    private int pageNum;
    @Schema(description = "分页大小")
    private int pageSize;
}
