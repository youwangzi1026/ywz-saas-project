package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.OperLogPageReq;

/**
 * 类描述 -> 系统操作日志业务
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
public interface OperLogApiService {
    /**
     * 方法描述 -> 分页查询系统操作日志
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    ResultResp getOperLogPage(OperLogPageReq req);

    /**
     * 方法描述 -> 删除系统操作日志
     *
     * @param ids 操作日志id
     * @Author: ywz
     * @Date: 2024/11/24
     */
    ResultResp removeOperLog(String ids);
}
