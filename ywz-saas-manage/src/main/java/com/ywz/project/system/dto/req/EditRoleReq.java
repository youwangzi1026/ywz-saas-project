package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 新增编辑角色请求
 *
 * @Author: ywz
 * @Date: 2024/11/21
 */
@Data
@Schema(description = "新增编辑角色请求")
public class EditRoleReq {
    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "角色名称")
    private String roleName;

    @Schema(description = "角色类型")
    private String roleType;

    @Schema(description = "角色描述")
    private String roleDesc;

    @Schema(description = "状态：1:可用；0:不可用")
    private Integer status;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "权限id集合")
    private String permissionIds;
}
