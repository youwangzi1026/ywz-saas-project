package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditPermissionReq;
import com.ywz.project.system.dto.req.PermissionPageReq;

/**
 * 类描述 -> 系统权限业务
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
public interface PermissionApiService {

    /**
     * 方法描述 -> 获取系统权限分页数据
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    ResultResp getPermissionPage(PermissionPageReq req);

    /**
     * 方法描述 -> 根据角色ID获取权限列表
     *
     * @param roleIds 角色ID
     * @Author: ywz
     * @Date: 2024/11/08
     */
    ResultResp getPermissionListByRoleId(String roleIds);

    /**
     * 方法描述 -> 新增、编辑权限
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/21
     */
    ResultResp editPermission(EditPermissionReq req);

    /**
     * 方法描述 -> 删除权限
     *
     * @param ids 权限ID
     * @Author: ywz
     * @Date: 2024/11/21
     */
    ResultResp removePermission(String ids);

    /**
     * 方法描述 -> 获取权限列表
     *
     * @Author: ywz
     * @Date: 2024/11/21
     */
    ResultResp getPermissionList();
}
