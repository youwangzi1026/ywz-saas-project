package com.ywz.project.system.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.LoginReq;
import com.ywz.project.system.dto.req.RegisterReq;
import com.ywz.project.system.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 类描述 -> 登录API
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Tag(name = "登录")
@RestController
@RequestMapping("/system")
public class LoginController {
    @Resource
    private LoginService loginService;
    @Value("${captcha.enabled}")
    private boolean captchaEnabled = false;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    // 用于创建验证码识别码
    private final Random random = new Random();

    /**
     * 方法描述 -> 登录
     *
     * @param req 登录参数
     * @Author: ywz
     * @Date: 2024/10/27
     */
    @PostMapping("/login")
    @Operation(summary = "登录")
    public ResultResp login(LoginReq req) {
        return loginService.login(req);
    }

    /**
     * 方法描述 -> 注册接口
     *
     * @Author: ywz
     * @Date: 2024/09/29
     */
    @PreAuthorize("hasRole('admin')")
    @PostMapping("/register")
    @Operation(summary = "注册企业用户接口")
    public ResultResp register(RegisterReq req) {
        return loginService.register(req);
    }

    /**
     * 方法描述 -> 退出登录接口
     *
     * @Author: ywz
     * @Date: 2024/09/29
     */
    @GetMapping("/logout")
    @Operation(summary = "退出登录接口")
    public ResultResp logout() {
        return loginService.logout();
    }

    /**
     * 方法描述 -> 重置密码接口
     *
     * @param oldPassword -> 旧密码
     * @param newPassword -> 新密码
     * @Author: ywz
     * @Date: 2024/10/09
     */
    @PostMapping("/resetPassword")
    @Operation(summary = "重置密码接口")
    public ResultResp resetPassword(String oldPassword, String newPassword) {
        return loginService.resetPassword(oldPassword, newPassword);
    }

    /**
     * 方法描述 -> 获取验证码
     *
     * @Author: ywz
     * @Date: 2024/11/03
     */
    @GetMapping("/captcha")
    @Operation(summary = "获取验证码")
    public ResultResp getCaptcha(HttpServletResponse response) throws IOException {
        if (!captchaEnabled)
            return ResultResp.error("验证码功能未开启");
        // 获取线性干扰验证码
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100, 4, 10);
        // 创建识别码
        String captchaKey = "";
        do {
            captchaKey = "key:" + random.nextInt(10000);
        } while (!Boolean.FALSE.equals(redisTemplate.hasKey(captchaKey)));

        // 将识别码与对应的验证码code存入redis
        redisTemplate.opsForValue().set(captchaKey, lineCaptcha.getCode(), 120, TimeUnit.SECONDS);
        response.setContentType("image/png");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setHeader("captchaKey", captchaKey);
        lineCaptcha.write(response.getOutputStream());
        return null;
    }
}
