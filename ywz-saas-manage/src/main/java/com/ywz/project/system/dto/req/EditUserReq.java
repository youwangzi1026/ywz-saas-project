package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 编辑用户请求参数
 *
 * @Author: ywz
 * @Date: 2024/11/22
 */
@Data
@Schema(description = "编辑用户请求参数")
public class EditUserReq {

    @Schema(description = "用户id")
    private Integer id;

    @Schema(description = "姓名")
    private String name;

    @Schema(description = "用户账号")
    private String Account;

    @Schema(description = "用户密码")
    private String password;

    @Schema(description = "部门id")
    private Integer deptId;

    @Schema(description = "角色id")
    private String roleIds;

    @Schema(description = "状态1启用2停用")
    private Integer status;

    @Schema(description = "备注")
    private String remark;
}
