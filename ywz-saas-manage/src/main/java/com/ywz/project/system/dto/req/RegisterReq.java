package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 注册请求参数
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Data
@Schema(description = "注册请求参数")
public class RegisterReq {

    @Schema(description = "姓名")
    private String name;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    private String password;
}
