package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 用户分页请求
 *
 * @Author: ywz
 * @Date: 2024/11/05
 */
@Data
@Schema(description = "用户分页请求")
public class UserPageReq {

    @Schema(description = "页码")
    private int pageNum;

    @Schema(description = "每页大小")
    private int pageSize;

    @Schema(description = "部门id")
    private Integer deptId;

    @Schema(description = "用户名")
    private String account;

    @Schema(description = "姓名")
    private String name;
}
