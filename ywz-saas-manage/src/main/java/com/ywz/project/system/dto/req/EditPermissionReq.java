package com.ywz.project.system.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 编辑权限请求
 *
 * @Author: ywz
 * @Date: 2025/02/07
 */
@Data
@Schema(description = "编辑权限请求")
public class EditPermissionReq {
    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "所属菜单")
    private String menuNames;

    @Schema(description = "所属子菜单")
    private String menuSubNames;

    @Schema(description = "权限名")
    private String permissionName;

    @Schema(description = "权限中文描述")
    private String permissionDesc;

    @Schema(description = "分组id")
    private Integer pid;

    @Schema(description = "是否是父级  1 父 0 子  2 操作")
    private Integer isParent;

    @Schema(description = "1增加 2查看 3修改 4 删除 5状态 6权限")
    private Integer isOperating;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "备注")
    private String remark;
}
