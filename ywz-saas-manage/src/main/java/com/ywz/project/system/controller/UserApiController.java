package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditUserReq;
import com.ywz.project.system.dto.req.UserPageReq;
import com.ywz.project.system.service.UserApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 类描述 -> 用户API
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@Tag(name = "用户管理")
@RestController
@RequestMapping("/system/user")
public class UserApiController {
    @Resource
    private UserApiService sysUserApiService;

    /**
     * 方法描述 -> 分页查询用户
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/05
     */
    @PreAuthorize("hasAuthority('PERM_USER_PAGE')")
    @GetMapping("/getUserPage")
    @Operation(summary = "分页查询用户")
    public ResultResp getUserPage(UserPageReq req) {
        return sysUserApiService.getUserPage(req);
    }

    /**
     * 方法描述 -> 编辑用户
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/23
     */
    @PreAuthorize("hasAuthority('PERM_USER_EDIT')")
    @PostMapping("/editUser")
    @Operation(summary = "编辑用户")
    public ResultResp editUser(EditUserReq req) {
        return sysUserApiService.editUser(req);
    }

    /**
     * 方法描述 -> 删除用户
     *
     * @param ids 用户id
     * @Author: ywz
     * @Date: 2024/11/23
     */
    @PreAuthorize("hasAuthority('PERM_USER_DELETE')")
    @DeleteMapping("/removeUser")
    @Operation(summary = "删除用户")
    public ResultResp removeUser(String ids) {
        return sysUserApiService.removeUser(ids);
    }
}
