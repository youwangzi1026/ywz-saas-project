package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.DepartmentPageReq;
import com.ywz.project.system.dto.req.EditDepartmentReq;

/**
 * 类描述 -> 部门业务接口
 *
 * @Author: ywz
 * @Date: 2024/11/22
 */
public interface DepartmentApiService {
    /**
     * 方法描述 -> 获取部门列表
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/22
     */
    ResultResp getDepartmentPage(DepartmentPageReq req);

    /**
     * 方法描述 -> 获取部门列表
     *
     * @Author: ywz
     * @Date: 2024/11/22
     */
    ResultResp getDepartmentList();

    /**
     * 方法描述 -> 新增部门
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/11/22
     */
    ResultResp editDepartment(EditDepartmentReq req);

    /**
     * 方法描述 -> 删除部门
     *
     * @param ids 请求参数
     * @Author: ywz
     * @Date: 2024/11/22
     */
    ResultResp removeDepartment(String ids);
}
