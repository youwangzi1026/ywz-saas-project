package com.ywz.project.system.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.DepartmentPageReq;
import com.ywz.project.system.dto.req.EditDepartmentReq;
import com.ywz.project.system.service.DepartmentApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 类描述 -> 部门管理
 *
 * @Author: ywz
 * @Date: 2024/11/22
 */
@RestController
@Tag(name = "部门管理")
@RequestMapping("/department")
public class DepartmentApiController {
    @Resource
    private DepartmentApiService departmentApiService;

    /**
     * 方法描述 -> 获取部门列表
     *
     * @param req -> 请求参数
     * @Author: ywz
     * @Date: 2024/11/22
     */
    @PreAuthorize("hasAuthority('PERM_DEPARTMENT_PAGE')")
    @GetMapping("/getDepartmentPage")
    @Operation(summary = "获取部门分页")
    public ResultResp getDepartmentPage(DepartmentPageReq req) {
        return departmentApiService.getDepartmentPage(req);
    }

    /**
     * 方法描述 -> 获取部门列表
     *
     * @Author: ywz
     * @Date: 2024/11/22
     */
    @PreAuthorize("hasAuthority('PERM_DEPARTMENT_LIST')")
    @GetMapping("/getDepartmentList")
    @Operation(summary = "获取部门列表")
    public ResultResp getDepartmentList() {
        return departmentApiService.getDepartmentList();
    }

    /**
     * 方法描述 -> 添加编辑部门
     *
     * @param req -> 请求参数
     * @Author: ywz
     * @Date: 2024/11/22
     */
    @PreAuthorize("hasAuthority('PERM_DEPARTMENT_EDIT')")
    @PostMapping("/editDepartment")
    @Operation(summary = "添加编辑部门")
    public ResultResp editDepartment(EditDepartmentReq req) {
        return departmentApiService.editDepartment(req);
    }

    /**
     * 方法描述 -> 删除部门
     *
     * @param ids -> 部门id
     * @Author: ywz
     * @Date: 2024/11/22
     */
    @PreAuthorize("hasAuthority('PERM_DEPARTMENT_DELETE')")
    @DeleteMapping("/removeDepartment")
    @Operation(summary = "删除部门")
    public ResultResp removeDepartment(String ids) {
        return departmentApiService.removeDepartment(ids);
    }
}
