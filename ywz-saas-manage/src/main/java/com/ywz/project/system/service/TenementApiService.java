package com.ywz.project.system.service;

import com.ywz.common.ResultResp;
import com.ywz.project.system.dto.req.EditTenementReq;
import com.ywz.project.system.dto.req.TenementPageReq;

/**
 * 类描述 -> 租户业务接口
 *
 * @Author: ywz
 * @Date: 2024/11/14
 */
public interface TenementApiService {
    /**
     * 方法描述 -> 登录界面获取租户信息
     *
     * @Author: ywz
     * @Date: 2024/11/14
     */
    ResultResp getTenementList();

    /**
     * 方法描述 -> 获取租户分页信息
     *
     * @param req 租户分页请求参数
     * @Author: ywz
     * @Date: 2024/11/15
     */
    ResultResp getTenementPage(TenementPageReq req);

    /**
     * 方法描述 -> 新增租户信息
     *
     * @param req 新增租户请求参数
     * @Author: ywz
     * @Date: 2024/11/16
     */
    ResultResp editTenement(EditTenementReq req);

    /**
     * 方法描述 -> 删除租户信息
     *
     * @param ids 租户id
     * @Author: ywz
     * @Date: 2024/11/19
     */
    ResultResp removeTenement(String ids);
}
