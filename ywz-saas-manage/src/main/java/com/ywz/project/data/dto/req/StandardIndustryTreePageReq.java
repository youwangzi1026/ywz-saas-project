package com.ywz.project.data.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 行业分类树分页请求参数
 *
 * @Author: ywz
 * @Date: 2025/02/07
 */
@Data
@Schema(description = "行业分类树分页请求参数")
public class StandardIndustryTreePageReq {
    @Schema(description = "页码")
    private int pageNum;
    @Schema(description = "每页条数")
    private int pageSize;
    @Schema(description = "行业名称")
    private String name;
}
