package com.ywz.project.data.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 行业编辑请求
 *
 * @Author: ywz
 * @Date: 2025/01/21
 */
@Data
@Schema(description = "行业编辑请求")
public class StandardIndustryEditReq {
    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "行业代码")
    private String code;

    @Schema(description = "行业名称")
    private String name;

    @Schema(description = "状态")
    private Integer status;

    @Schema(description = "父类ID")
    private String parentId;
}
