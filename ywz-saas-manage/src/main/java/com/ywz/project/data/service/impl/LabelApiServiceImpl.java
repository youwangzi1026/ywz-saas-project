package com.ywz.project.data.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ywz.common.ResultResp;
import com.ywz.common.StringUtils;
import com.ywz.project.base.data.entity.TDataLabel;
import com.ywz.project.base.data.service.TDataLabelService;
import com.ywz.project.data.dto.req.EditLabelReq;
import com.ywz.project.data.dto.req.LabelPageReq;
import com.ywz.project.data.service.LabelApiService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LabelApiServiceImpl implements LabelApiService {
    @Resource
    private TDataLabelService tLabelService;

    @Override
    public ResultResp getLabelListByType(String type) {
        LambdaQueryWrapper<TDataLabel> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TDataLabel::getType, type);
        queryWrapper.eq(TDataLabel::getIsDeleted, 0);
        return ResultResp.success(tLabelService.list(queryWrapper));
    }

    @Override
    public ResultResp getLabelPage(LabelPageReq req) {
        LambdaQueryWrapper<TDataLabel> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(TDataLabel::getIsDeleted, 0);
        queryWrapper.ne(TDataLabel::getType, "tenement_key"); // 不查询租户标签
        if (!StringUtils.isEmpty(req.getName()))
            queryWrapper.like(TDataLabel::getName, req.getName());
        if (!StringUtils.isEmpty(req.getType()))
            queryWrapper.eq(TDataLabel::getType, req.getType());
        return ResultResp.success(tLabelService.page(new Page<>(req.getPageNum(), req.getPageSize()), queryWrapper));
    }

    @Override
    public ResultResp editLabel(EditLabelReq req) {
        if (StringUtils.isEmpties(req.getName(), req.getTag(), req.getType()))
            return ResultResp.error("参数为空");
        if (StringUtils.isOverLength(255, req.getName(), req.getTag(), req.getRemark(), req.getType()))
            return ResultResp.error("参数长度过长");
        TDataLabel label = new TDataLabel();
        BeanUtils.copyProperties(req, label);
        label.setUpdatedTime(LocalDateTime.now());
        boolean saved = tLabelService.saveOrUpdate(label);
        return saved ? ResultResp.success() : ResultResp.error();
    }

    @Override
    public ResultResp removeLabel(String ids) {
        if (!StringUtils.isNumberAndSeparator(ids))
            return ResultResp.error("参数错误");
        String[] split = ids.split(StringUtils.SEPARATOR);
        List<Integer> collect = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
        LambdaUpdateWrapper<TDataLabel> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.in(TDataLabel::getId, collect);
        updateWrapper.set(TDataLabel::getIsDeleted, 1);
        boolean updated = tLabelService.update(updateWrapper);
        return updated ? ResultResp.success() : ResultResp.error();
    }

}
