package com.ywz.project.data.service;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.EditLabelReq;
import com.ywz.project.data.dto.req.LabelPageReq;

/**
 * 类描述 -> 标签API业务接口
 *
 * @Author: ywz
 * @Date: 2024/11/22
 */
public interface LabelApiService {
    /**
     * 方法描述 -> 根据类型获取标签列表
     *
     * @param type 标签类型
     * @Author: ywz
     * @Date: 2024/11/23
     */
    ResultResp getLabelListByType(String type);

    /**
     * 方法描述 -> 获取标签分页列表
     *
     * @param req 标签分页请求
     * @Author: ywz
     * @Date: 2024/11/23
     */
    ResultResp getLabelPage(LabelPageReq req);

    /**
     * 方法描述 -> 删除标签
     *
     * @param ids 标签ID
     * @Author: ywz
     * @Date: 2024/11/23
     */
    ResultResp removeLabel(String ids);

    /**
     * 方法描述 -> 新增标签
     *
     * @param req 标签信息
     * @Author: ywz
     * @Date: 2024/11/23
     */
    ResultResp editLabel(EditLabelReq req);
}
