package com.ywz.project.data.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.StandardIndustryEditReq;
import com.ywz.project.data.dto.req.StandardIndustryTreePageReq;
import com.ywz.project.data.service.StandardIndustryApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 类描述 -> 国民行业分类
 *
 * @Author: ywz
 * @Date: 2024/12/30
 */
@Tag(name = "国民行业分类")
@RestController
@RequestMapping("/standardIndustry")
public class StandardIndustryApiController {
    @Resource
    private StandardIndustryApiService standardIndustryApiService;

    /**
     * 方法描述 -> 获取行业分类树
     *
     * @Author: ywz
     * @Date: 2024/12/30
     */
    @PreAuthorize("hasAuthority('PERM_STANDARD_INDUSTRY_TREE')")
    @GetMapping("/getStandardIndustryTree")
    @Operation(summary = "获取行业分类树")
    public ResultResp getStandardIndustryTree() {
        return standardIndustryApiService.getStandardIndustryTree();
    }


    /**
     * 方法描述 -> 获取行业分类树
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/12/31
     */
    @PreAuthorize("hasAuthority('PERM_STANDARD_INDUSTRY_TREE_PAGE')")
    @GetMapping("/getStandardIndustryTreePage")
    @Operation(summary = "获取行业分类树")
    public ResultResp getStandardIndustryTreePage(StandardIndustryTreePageReq req) {
        return standardIndustryApiService.getStandardIndustryTreePage(req);
    }

    /**
     * 方法描述 -> 编辑行业分类
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/01/21
     */
    @PreAuthorize("hasAuthority('PERM_STANDARD_INDUSTRY_EDIT')")
    @PostMapping("/editStandardIndustry")
    @Operation(summary = "编辑行业分类")
    public ResultResp editStandardIndustry(StandardIndustryEditReq req) {
        return standardIndustryApiService.editStandardIndustry(req);
    }

    /**
     * 方法描述 -> 删除行业分类
     *
     * @param id 行业分类id
     * @Author: ywz
     * @Date: 2025/01/21
     */
    @PreAuthorize("hasAuthority('PERM_STANDARD_INDUSTRY_DELETE')")
    @DeleteMapping("/deleteStandardIndustry")
    @Operation(summary = "删除行业分类")
    public ResultResp deleteStandardIndustry(Integer id) {
        return standardIndustryApiService.deleteStandardIndustry(id);
    }
}
