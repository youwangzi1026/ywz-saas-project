package com.ywz.project.data.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.EditLabelReq;
import com.ywz.project.data.dto.req.LabelPageReq;
import com.ywz.project.data.service.LabelApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * 类描述 -> 标签API
 *
 * @Author: ywz
 * @Date: 2024/11/22
 */
@Tag(name = "标签管理")
@RestController
@RequestMapping("/system/label")
public class LabelApiController {
    @Resource
    private LabelApiService labelApiService;

    /**
     * 方法描述 -> 获取所有标签列表
     *
     * @param type 标签类型
     * @Author: ywz
     * @Date: 2024/11/23
     */
    @PreAuthorize("hasAuthority('PERM_LABEL_LIST_BY_TYPE')")
    @GetMapping("/getLabelListByType")
    @Operation(summary = "根据类型获取标签列表")
    public ResultResp getLabelListByType(String type) {
        return labelApiService.getLabelListByType(type);
    }

    /**
     * 方法描述 -> 获取标签分页列表
     *
     * @param req 标签分页请求
     * @Author: ywz
     * @Date: 2024/11/23
     */
    @PreAuthorize("hasAuthority('PERM_LABEL_PAGE')")
    @GetMapping("/getLabelPage")
    @Operation(summary = "获取标签分页列表")
    public ResultResp getLabelPage(LabelPageReq req){
        return labelApiService.getLabelPage(req);
    }

    /**
     * 方法描述 -> 新增编辑标签
     *
     * @param req 标签请求
     * @Author: ywz
     * @Date: 2024/11/23
     */
    @PreAuthorize("hasAuthority('PERM_LABEL_EDIT')")
    @PostMapping("/editLabel")
    @Operation(summary = "编辑标签")
    public ResultResp editLabel(EditLabelReq req){
        return labelApiService.editLabel(req);
    }

    /**
     * 方法描述 -> 删除标签
     *
     * @param ids 标签ID
     * @Author: ywz
     * @Date: 2024/11/23
     */
    @PreAuthorize("hasAuthority('PERM_LABEL_DELETE')")
    @DeleteMapping("/removeLabel")
    @Operation(summary = "删除标签")
    public ResultResp removeLabel(String ids){
        return labelApiService.removeLabel(ids);
    }
}
