package com.ywz.project.data.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 编辑标签
 *
 * @Author: ywz
 * @Date: 2025/02/07
 */
@Data
@Schema(description = "编辑标签")
public class EditLabelReq {
    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "标签名")
    private String name;

    @Schema(description = "标签标识")
    private String tag;

    @Schema(description = "标签分类")
    private String type;

    @Schema(description = "父级标签ID")
    private Integer pid;

    @Schema(description = "备注")
    private String remark;
}
