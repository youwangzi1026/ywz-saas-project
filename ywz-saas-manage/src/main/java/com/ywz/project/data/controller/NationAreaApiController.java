package com.ywz.project.data.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.NationAreaEditReq;
import com.ywz.project.data.dto.req.NationAreaTreeListPageReq;
import com.ywz.project.data.service.NationAreaApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 类描述 -> 国家地区API
 *
 * @Author: ywz
 * @Date: 2025/02/28
 */
@Tag(name = "国家地区")
@RestController
@RequestMapping("/nationArea")
public class NationAreaApiController {

    @Resource
    private NationAreaApiService nationAreaApiService;

    /**
     * 方法描述 ->  根据id获取中国地区树形结构
     *
     * @Author: ywz
     * @Date: 2024/10/27
     */
    @PreAuthorize("hasAuthority('PERM_NATION_AREA_TREE_LIST')")
    @GetMapping("/getNationAreaTreeList")
    @Operation(summary = "根据id获取中国地区树形结构")
    public ResultResp getNationAreaTreeList() {
        return nationAreaApiService.getNationAreaTreeList();
    }

    /**
     * 方法描述 -> 获取中国地区树形结构分页列表
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/12/30
     */
    @PreAuthorize("hasAuthority('PERM_NATION_AREA_TREE_PAGE')")
    @GetMapping("/getNationAreaTreeListPage")
    @Operation(summary = "获取中国地区树形结构分页列表")
    public ResultResp getNationAreaTreeListPage(NationAreaTreeListPageReq req) {
        return nationAreaApiService.getNationAreaTreeListPage(req);
    }

    /**
     * 方法描述 -> 编辑中国地区
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/12/31
     */
    @PreAuthorize("hasAuthority('PERM_NATION_AREA_EDIT')")
    @PostMapping("/editNationArea")
    @Operation(summary = "编辑中国地区")
    public ResultResp editNationArea(NationAreaEditReq req) {
        return nationAreaApiService.editNationArea(req);
    }

    /**
     * 方法描述 -> 删除中国地区
     *
     * @param id 地区id
     * @Author: ywz
     * @Date: 2024/12/31
     */
    @PreAuthorize("hasAuthority('PERM_NATION_AREA_DELETE')")
    @DeleteMapping("/deleteNationArea")
    @Operation(summary = "删除中国地区")
    public ResultResp deleteNationArea(Integer id) {
        return nationAreaApiService.deleteNationArea(id);
    }
}
