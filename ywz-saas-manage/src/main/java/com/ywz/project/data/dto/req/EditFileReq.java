package com.ywz.project.data.dto.req;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 编辑文件
 *
 * @Author: ywz
 * @Date: 2025/01/06
 */
@Data
@Schema(description = "编辑文件")
public class EditFileReq {
    @Schema(description = "主键")
    private Integer id;

    @Schema(description = "文件名")
    private String name;

    @Schema(description = "文件类型")
    private String type;

    @Schema(description = "标签id")
    private Integer labelId;

    @Schema(description = "文件位置")
    private String fileUrl;

    @Schema(description = "备注")
    @TableField("REMARK")
    private String remark;
}
