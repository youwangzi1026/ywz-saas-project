package com.ywz.project.data.service;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.StandardIndustryEditReq;
import com.ywz.project.data.dto.req.StandardIndustryTreePageReq;


/**
 * 类描述 -> 行业API
 *
 * @Author: ywz
 * @Date: 2024/12/30
 */
public interface StandardIndustryApiService {

    /**
     * 方法描述 -> 获取行业树
     *
     * @Author: ywz
     * @Date: 2024/12/30
     */
    ResultResp getStandardIndustryTree();

    /**
     * 方法描述 -> 获取行业树分页
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2024/12/31
     */
    ResultResp getStandardIndustryTreePage(StandardIndustryTreePageReq req);

    /**
     * 方法描述 -> 删除行业
     *
     * @param id 行业id
     * @Author: ywz
     * @Date: 2025/01/21
     */
    ResultResp deleteStandardIndustry(Integer id);

    /**
     * 方法描述 -> 编辑行业
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/01/21
     */
    ResultResp editStandardIndustry(StandardIndustryEditReq req);
}
