package com.ywz.project.data.controller;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.EditFileReq;
import com.ywz.project.data.dto.req.FilePageReq;
import com.ywz.project.data.service.FileApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * 类描述 -> 文件管理
 *
 * @Author: ywz
 * @Date: 2025/01/06
 */
@Tag(name = "文件管理")
@RestController
@RequestMapping("file")
public class FileApiController {
    @Resource
    private FileApiService fileApiService;

    /**
     * 方法描述 -> 上传文件
     *
     * @param file 文件
     * @param delFileName 同时要删除的文件
     * @Author: ywz
     * @Date: 2025/01/06
     */
    @PreAuthorize("hasAuthority('PERM_FILE_UPLOAD')")
    @PostMapping("/uploadFile")
    @Operation(summary = "上传文件")
    public ResultResp uploadFile(MultipartFile file, String delFileName) {
        return fileApiService.uploadFile(file, delFileName);
    }

    /**
     * 方法描述 -> 获取文件列表
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/01/06
     */
    @PreAuthorize("hasAuthority('PERM_FILE_PAGE')")
    @GetMapping("/getFilePage")
    @Operation(summary = "获取文件列表")
    public ResultResp getFilePage(FilePageReq req) {
        return fileApiService.getFilePage(req);
    }

    /**
     * 方法描述 -> 编辑文件
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/01/06
     */
    @PreAuthorize("hasAuthority('PERM_FILE_EDIT')")
    @PostMapping("/editFile")
    @Operation(summary = "编辑文件")
    public ResultResp editFile(EditFileReq req) {
        return fileApiService.editFile(req);
    }


    /**
     * 方法描述 -> 下载文件
     *
     * @param name 文件名
     * @param contentType 文件类型
     * @param response 响应
     * @Author: ywz
     * @Date: 2025/01/06
     */
    @PreAuthorize("hasAuthority('PERM_FILE_DOWNLOAD')")
    @GetMapping("/downloadFile")
    @Operation(summary = "下载文件")
    public ResultResp downloadFile(String name, String contentType, HttpServletResponse response) {
        return fileApiService.downloadFile(name, contentType, response);
    }

    /**
     * 方法描述 -> 删除文件，此接口会删除文件
     *
     * @param name 文件名
     * @Author: ywz
     * @Date: 2025/01/06
     */
    @PreAuthorize("hasAuthority('PERM_FILE_DELETE')")
    @DeleteMapping("/deleteFile")
    @Operation(summary = "删除文件")
    public ResultResp deleteFile(String name) {
        return fileApiService.deleteFile(name);
    }

    /**
     * 方法描述 -> 删除文件记录信息
     *
     * @param ids 文件id
     * @Author: ywz
     * @Date: 2025/01/06
     */
    @PreAuthorize("hasAuthority('PERM_FILE_REMOVE')")
    @DeleteMapping("/removeFile")
    @Operation(summary = "删除文件")
    public ResultResp removeFile(String ids) {
        return fileApiService.removeFile(ids);
    }
}
