package com.ywz.project.data.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 标签分页请求
 *
 * @Author: ywz
 * @Date: 2024/11/23
 */
@Data
@Schema(description = "标签分页请求")
public class LabelPageReq {
    @Schema(description = "标签名称")
    private String name;
    @Schema(description = "标签类型")
    private String type;
    @Schema(description = "当前页码")
    private int pageNum;
    @Schema(description = "每页条数")
    private int pageSize;
}
