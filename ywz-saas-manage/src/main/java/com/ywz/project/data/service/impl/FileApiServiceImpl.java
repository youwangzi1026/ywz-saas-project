package com.ywz.project.data.service.impl;

import cn.hutool.jwt.JWT;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ywz.common.JWTUtils;
import com.ywz.common.ResultResp;
import com.ywz.common.StringUtils;
import com.ywz.framework.minio.MinioService;
import com.ywz.project.base.data.entity.TDataFile;
import com.ywz.project.base.data.service.TDataFileService;
import com.ywz.project.data.dto.req.EditFileReq;
import com.ywz.project.data.dto.req.FilePageReq;
import com.ywz.project.data.mapper.FileApiMapper;
import com.ywz.project.data.service.FileApiService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileApiServiceImpl implements FileApiService {
    @Resource
    private FileApiMapper fileApiMapper;
    @Resource
    private TDataFileService TDataFileService;
    @Resource
    private MinioService minioService;

    @Override
    public ResultResp uploadFile(MultipartFile file, String delFileName) {
        ResultResp resultResp = minioService.uploadFile(file);
        if (resultResp.getCode() == 200) {
            minioService.deleteFile(delFileName);
        }
        return resultResp;
    }

    @Override
    public ResultResp getFilePage(FilePageReq req) {
        Page<TDataFile> page = fileApiMapper.getFilePage(new Page<>(req.getPageNum(), req.getPageSize()), req);
        return ResultResp.success(page);
    }

    @Override
    public ResultResp editFile(EditFileReq req) {
        // 数据校验
        if (StringUtils.isEmpties(req.getName(), req.getType(), req.getFileUrl()))
            return ResultResp.error("参数不能为空");
        if (StringUtils.isNull(req.getLabelId()))
            return ResultResp.error("标签不能为空");
        if (StringUtils.isOverLength(255, req.getName(), req.getType(), req.getFileUrl(), req.getRemark()))
            return ResultResp.error("参数长度过长");
        TDataFile file = new TDataFile();
        BeanUtils.copyProperties(req, file);
        if (StringUtils.isNull(req.getId())) {
            file.setCreatedTime(LocalDateTime.now());
        } else {
            file.setUpdatedTime(LocalDateTime.now());
        }
        JWT currentTokenInfo = JWTUtils.getCurrentTokenInfo();
        if (currentTokenInfo != null) {
            Object name = currentTokenInfo.getPayload("username");
            if (name != null) {
                file.setUpdatedBy(name.toString());
            }
        }
        boolean saved = TDataFileService.saveOrUpdate(file);
        return saved ? ResultResp.success() : ResultResp.error("保存失败");
    }

    @Override
    public ResultResp downloadFile(String name, String contentType, HttpServletResponse response) {
        return minioService.downloadFile(name, contentType, response);
    }

    @Override
    public ResultResp deleteFile(String name) {
        return minioService.deleteFile(name);
    }

    @Override
    public ResultResp removeFile(String ids) {
        // TODO
        if (StringUtils.isNumberAndSeparator(ids)) {
            List<Integer> collect = Arrays.stream(ids.split(StringUtils.SEPARATOR)).map(Integer::valueOf).collect(Collectors.toList());
            LambdaQueryWrapper<TDataFile> wrapper = new LambdaQueryWrapper<>();
            wrapper.in(TDataFile::getId, collect);
            List<TDataFile> list = TDataFileService.list(wrapper);
            for (TDataFile TDataFile : list)
                minioService.deleteFile(TDataFile.getFileUrl());
            return TDataFileService.removeByIds(collect) ? ResultResp.success() : ResultResp.error("删除失败");
        }
        return ResultResp.error("参数格式错误");
    }

    @Override
    public ResultResp downloadFileByTenementKey(String name, String contentType, String key, HttpServletResponse response) {
        return minioService.downloadFile(name, contentType, key, response);
    }
}
