package com.ywz.project.data.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ywz.project.base.data.entity.TDataFile;
import com.ywz.project.data.dto.req.FilePageReq;

public interface FileApiMapper {
    Page<TDataFile> getFilePage(Page<TDataFile> objectPage, FilePageReq req);
}
