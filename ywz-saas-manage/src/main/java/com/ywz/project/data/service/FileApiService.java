package com.ywz.project.data.service;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.EditFileReq;
import com.ywz.project.data.dto.req.FilePageReq;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;


/**
 * 类描述 -> 文件管理
 *
 * @Author: ywz
 * @Date: 2025/01/06
 */
public interface FileApiService {
    /**
     * 方法描述 -> 上传文件
     *
     * @param file 文件
     * @Author: ywz
     * @Date: 2025/01/06
     */
    ResultResp uploadFile(MultipartFile file, String delFileName);

    /**
     * 方法描述 -> 分页查询文件列表
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/01/06
     */
    ResultResp getFilePage(FilePageReq req);

    /**
     * 方法描述 -> 编辑文件
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/01/06
     */
    ResultResp editFile(EditFileReq req);

    /**
     * 方法描述 -> 删除文件
     *
     * @param name 文件名
     * @Author: ywz
     * @Date: 2025/01/06
     */
    ResultResp downloadFile(String name, String contentType, HttpServletResponse response);

    /**
     * 方法描述 -> 删除文件
     *
     * @param name 文件名
     * @Author: ywz
     * @Date: 2025/01/06
     */
    ResultResp deleteFile(String name);

    /**
     * 方法描述 -> 批量删除文件
     *
     * @param ids 文件ids
     * @Author: ywz
     * @Date: 2025/01/06
     */
    ResultResp removeFile(String ids);

    /**
     * 方法描述 -> 根据租户key下载文件
     *
     * @param name 文件名
     * @param contentType 文件类型
     * @param key 租户key
     * @param response 响应
     * @Author: ywz
     * @Date: 2025/01/08
     */
    ResultResp downloadFileByTenementKey(String name, String contentType, String key, HttpServletResponse response);
}
