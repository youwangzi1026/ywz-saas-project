package com.ywz.project.data.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 编辑省市区
 *
 * @Author: ywz
 * @Date: 2024/12/31
 */
@Data
@Schema(description = "编辑省市区")
public class NationAreaEditReq {
    @Schema(description = "id")
    private Integer id;

    @Schema(description = "行政区划代码")
    private Integer code;

    @Schema(description = "名称")
    private String name;

    @Schema(description = "上级id")
    private Integer pid;

    @Schema(description = "类型")
    private String type;

    @Schema(description = "经度")
    private String longitude;

    @Schema(description = "纬度")
    private String latitude;
}
