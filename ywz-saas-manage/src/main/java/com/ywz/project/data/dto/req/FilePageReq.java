package com.ywz.project.data.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 文件分页请求参数
 *
 * @Author: ywz
 * @Date: 2025/01/06
 */
@Data
@Schema(description = "文件分页请求参数")
public class FilePageReq {
    @Schema(description = "当前页")
    private int pageNum;
    @Schema(description = "每页数量")
    private int pageSize;
    @Schema(description = "文件名称")
    private String name;
    @Schema(description = "文件标签id")
    private Integer labelId;
}
