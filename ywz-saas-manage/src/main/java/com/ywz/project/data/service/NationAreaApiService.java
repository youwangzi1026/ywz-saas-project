package com.ywz.project.data.service;

import com.ywz.common.ResultResp;
import com.ywz.project.data.dto.req.NationAreaEditReq;
import com.ywz.project.data.dto.req.NationAreaTreeListPageReq;

/**
 * 类描述 -> 国家行政区划信息表
 *
 * @Author: ywz
 * @Date: 2025/02/28
 */
public interface NationAreaApiService {
    /**
     * 方法描述 -> 获取国家行政区划树
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/02/28
     */
    ResultResp getNationAreaTreeListPage(NationAreaTreeListPageReq req);

    /**
     * 方法描述 -> 编辑国家行政区划信息
     *
     * @param req 请求参数
     * @Author: ywz
     * @Date: 2025/02/28
     */
    ResultResp editNationArea(NationAreaEditReq req);

    /**
     * 方法描述 -> 删除国家行政区划信息
     *
     * @param id 国家行政区划信息id
     * @Author: ywz
     * @Date: 2025/02/28
     */
    ResultResp deleteNationArea(Integer id);

    /**
     * 方法描述 -> 根据id获取国家行政区划树
     *
     * @Author: ywz
     * @Date: 2025/02/28
     */
    ResultResp getNationAreaTreeList();
}
