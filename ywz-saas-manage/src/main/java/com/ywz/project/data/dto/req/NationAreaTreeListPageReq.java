package com.ywz.project.data.dto.req;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 类描述 -> 中国地区树形结构分页列表请求参数
 *
 * @Author: ywz
 * @Date: 2024/12/30
 */
@Data
@Schema(description = "中国地区树形结构分页列表请求参数")
public class NationAreaTreeListPageReq {
    @Schema(description = "页码")
    private int pageNum;
    @Schema(description = "每页数量")
    private int pageSize;
    @Schema(description = "名称")
    private String name;
}
