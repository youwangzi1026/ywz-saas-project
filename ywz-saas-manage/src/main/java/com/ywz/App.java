package com.ywz;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 类描述 -> 启动类
 *
 * @Author: ywz
 * @Date: 2024/10/27
 */
@SpringBootApplication
@MapperScan("com.ywz.project.**.mapper")
@ServletComponentScan // 扫描servlet注解
@OpenAPIDefinition(info = @Info(title = "游王子og管理平台", description = "简洁的管理系统框架，适用于常见的快速开发场景。", version = "1.0.0"))
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        System.out.println("================系统启动成功！游王子og博客 https://youwangzi.blog.csdn.net/ ==================");
    }
}
