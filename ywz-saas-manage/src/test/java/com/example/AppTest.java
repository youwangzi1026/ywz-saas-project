package com.example;


import org.apache.commons.lang3.math.NumberUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;

/**
 * Unit test for simple App.
 */
public class AppTest {
    private static final String REDIS_KEY_UPDATE_DATA_SET = "analysis:zhaoshang:update:data:set";

    public static void main(String[] args) throws IOException {
        Reader input = new FileReader("D://company-20230815-part-00000-748d1b75-6ffd-4f40-bb1b-b18fa669aa03.c000.sql");
        try {
            BufferedReader reader = new BufferedReader(input);
            String str;
            AtomicInteger num = new AtomicInteger(0);
            while (true) {
                num.getAndIncrement();
                //按行读取
                str = reader.readLine();
                if (str != null) {
                    try {
                        if (str.startsWith("update") && str.contains("`cid`='")) {
                            String replace = str.replace(" ", "");
                            String substring = replace.substring(replace.indexOf("`cid`='") + 7);
                            String s = substring.substring(0, substring.indexOf("',`"));
                            if (NumberUtils.isDigits(s)) {
                                System.out.println("update redis k = " + REDIS_KEY_UPDATE_DATA_SET);
                                System.out.println("update redis v = " + s);
                            }
                        } else if (str.startsWith("insert") && str.contains("`cid`")) {
                            System.out.println(str);
                            String replace = str.replace(" ", "");
                            String sub2 = replace.substring(replace.indexOf("values("));
                            String sub3 = sub2.substring(sub2.indexOf("','") + 3);
                            String sub4 = sub3.substring(0, sub3.indexOf("','"));
                            if (NumberUtils.isDigits(sub4)) {
                                System.out.println("=====insert redis k = " + REDIS_KEY_UPDATE_DATA_SET);
                                System.out.println("=====insert redis v = " + sub4);
                            }
                        }
                    } catch (Exception ignored) {
                    }
                } else {
                    break;
                }
            }
        } catch (Exception ignored) {
        } finally {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ignored) {
            }

            try {
                input.close();
            } catch (IOException ignored) {
            }
        }
    }
}
