package com.ywz.encryption;

import com.ywz.common.SymmetricAlgorithmUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 类描述 -> 对称加密算法测试类
 *
 * @Author: ywz
 * @Date: 2025/01/22
 */
@SpringBootTest
public class SymmetricAlgorithmTest {
    @Test
    public void test() {
        String content = "对称加密算法";
        System.out.println("原文：" + content);
        String encrypt = SymmetricAlgorithmUtils.encrypt(content);
        System.out.println("加密：" + encrypt);
        String decrypt = SymmetricAlgorithmUtils.decrypt(encrypt);
        System.out.println("解密：" + decrypt);
    }
}
