package com.ywz.encryption;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.ywz.common.AsymmetricCryptoUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 类描述 -> 非对称加密算法测试类
 *
 * @Author: ywz
 * @Date: 2025/01/22
 */
@SpringBootTest
public class AsymmetricCryptoTest {
    @Test
    public void test() {
        String content = "111222ABC";
        String encrypt = AsymmetricCryptoUtils.encrypt(content, KeyType.PublicKey);
        System.out.println("加密后：" + encrypt);
        String decrypt = AsymmetricCryptoUtils.decrypt(encrypt, KeyType.PrivateKey);
        System.out.println("解密后：" + decrypt);
    }

    /**
     * 方法描述 -> 获取公私钥
     *
     * @Author: ywz
     * @Date: 2025/01/22
     */
    @Test
    public void getKey() {
        RSA rsa = new RSA();
        //获得私钥
        System.out.println(rsa.getPrivateKeyBase64());
        System.out.println("==================分割线====================");
        //获得公钥
        System.out.println(rsa.getPublicKeyBase64());
    }
}
