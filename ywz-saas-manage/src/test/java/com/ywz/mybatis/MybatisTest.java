package com.ywz.mybatis;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MybatisTest {

    @Test
    public void test() {
        String key = "YouWangZi1234567";
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key.getBytes(CharsetUtil.CHARSET_UTF_8));
        //加密为16进制表示
        String encryptHex = aes.encryptHex("root");
        System.out.println("root加密后：" + encryptHex);
        String url  = "jdbc:mysql://localhost:3306/saas-crm-master";
        String urlEncryptHex = aes.encryptHex(url);
        System.out.println("url加密后：" + urlEncryptHex);
        //解密为字符串
        String decryptStr = aes.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);
        System.out.println("root解密：" + decryptStr);
        String urlDecryptStr = aes.decryptStr(urlEncryptHex, CharsetUtil.CHARSET_UTF_8);
        System.out.println("url解密：" + urlDecryptStr);
    }
}
